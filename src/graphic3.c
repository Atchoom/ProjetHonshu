#include <stdlib.h>
#include <gtk/gtk.h>
#include "init.h"
#include "score.h"
#include "affichage.h"
#include "recouvrement.h"
#include "solver.h"


/* Pour compiler : gcc graphic3.c ../src/pile.c ../src/init.c ../src/list.c ../src/grid.c ../src/score.c ../src/affichage.c ../src/recouvrement.c ../src/solver.c `pkg-config --cflags --libs gtk+-3.0` -o interface
*/

/*Les fonctions */

GtkWidget* get_image_square(char c);

GtkWidget* gprint_tile(tile t);

void test(GtkWidget *button, gpointer user_data);

void add_tile(GtkWidget *button, gpointer user_data);

void position_tile(GtkWidget *widget, gpointer user_data);

void selection_tile(GtkWidget *widget, gpointer user_data);

void menu_selection(GtkWidget *widget, gpointer user_data);

void rotation_tile(GtkWidget *widget, gpointer user_data);

void print_end();

void destroy_window(GtkWidget *widget, gpointer user_data);

void advice(GtkWidget *widget, gpointer user_data);

void gprint_rules(GtkWidget *widget, gpointer user_data);




/*Struct pour callback*/
struct gcase {
    GtkWidget *main;
    GtkWidget *child;
    int x;
};

/*Variables globales*/

int is_selecting = 0;/*Pour savoir si on est en mode sélection ou non*/
int id_tile_selected = 0;/*L'id de la tuile actuellement sélectionnée lors du mode sélection*/
int selected_x = 0;/*Les coordonnées sélectionnées pour poser la tuile*/
int selected_y = 0;
int overlay = 0;/*Si on est entrain de prévisualiser la pose de tuile*/
int id_tiles[12];/*tableau des numéros de tuiles*/


GtkWidget *grid_table;/*Le widget de la grille de jeu*/
GtkWidget *fixe;/*Le widget de l'overlay permettant la previsualisation de la pose*/
GtkWidget *Overlay_contour;/*Le widget de la tuile prévisualisée*/
GtkWidget *vbox_menu_posage;/*Le menu pour poser une tuile*/
GtkWidget *pFrame_main;/*Le widget contenant l'affichage de la main*/
GtkWidget *bouton_main[12];/*Le tableau des boutons de sélection des tuiles*/
tile selected_tile;/*La tuile sélectionnée*/
game g;/*La partie en cours*/




int main(int argc,char **argv)
{
    int i,j;/*itérateurs*/
    int n;/*Taille de la grille*/

    /* Déclaration des widgets */
    GtkWidget *pWindow;/*La fenêtre principale*/
    GtkWidget *hand_table;/*la grille pour l'affichage de la main*/
    GtkWidget  *vbox1,*hbox1, *vbox_menu1;/*Les différentes Box*/
    GtkWidget *gtile;/*Le widget pour l'affichage des tuiles de la main*/

    GtkWidget *pFrame_grid,*pFrame_selec;/*Les widgets Frame*/
    GtkWidget * pSeparator;/*Le widget de la barre vertical de séparation*/
    GtkWidget *previsu; /*Le widget de l'overlay*/

    GtkWidget *bouton1,*bouton2,*bouton3;/*Les différents boutons du menu*/
    GtkWidget *bouton_pose, *bouton_rotate, *bouton_rules;/*Les widgets des boutons du sous-menu du mode sélection*/

    /*On stock les id des tuiles*/
    for(i=1;i<13;i++)
    {
        id_tiles[i] = i;
    }

    /*Initialise l'interface*/
    gtk_init(&argc,&argv);
    /*Initialisation de la game, faute de menu, elle sera random avec les règles de base*/
    g = init_game(0);
    /*On place par défaut la première tuile de la main comme tuile sélectionnée*/
    selected_tile = display_list(g.hand,0);

    /* Création de la fenêtre */
    pWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);

    /* Titre de la fenêtre, taille de la fenetre et position */
    gtk_window_set_position(GTK_WINDOW(pWindow),GTK_WIN_POS_CENTER);
    gtk_window_set_title(GTK_WINDOW(pWindow),"Honshu");
    gtk_container_set_border_width(GTK_CONTAINER(pWindow), 5);

    /* Connexion du signal "destroy" a la fermeture de la fenetre */
    g_signal_connect(G_OBJECT(pWindow), "destroy", G_CALLBACK(gtk_main_quit), NULL);

    /*Creation d'une box verticale, container principale*/
    vbox1 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 1);
    gtk_container_add (GTK_CONTAINER (pWindow), vbox1);/*Ajout à la fenêtre*/


    /*Creation des différentes parties de l'interface*/


    /*Creation de la box horizontale pour contenir la grille et le menu de jeu*/
    hbox1 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 1);
    gtk_container_add(GTK_CONTAINER(vbox1),GTK_WIDGET(hbox1));
    /*Creation des frames des deux objets de la box*/
    pFrame_selec = gtk_frame_new("Fenêtre d'action:");
    gtk_frame_set_shadow_type (GTK_FRAME (pFrame_selec), GTK_SHADOW_IN);
    pFrame_grid = gtk_frame_new("Grille de jeu:");
    gtk_frame_set_shadow_type (GTK_FRAME (pFrame_grid), GTK_SHADOW_IN);

    /*Creation de la grille de jeu*/
    n = g.lg;/*initialisation de la taille de la grille*/
    pile p;/*Pile temporaire pour stocker les cases*/
    GtkWidget *image;/*Widget image pour stocker les images des terrains*/
    GtkWidget * EventBox[n][n];/*Tableau d'EventBox*/
    grid_table = gtk_grid_new();/*Creation de la grille*/
    struct gcase user_data[n];/*Creation de la structure pour transmettre les données via un signal*/
    /*Pour chaque case de la grille*/
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            /*On créé une EventBox dans le tableau*/
            EventBox[i][j] = gtk_event_box_new();
            gtk_event_box_set_above_child(GTK_EVENT_BOX(EventBox[i][j]),TRUE);
            gtk_event_box_set_visible_window (GTK_EVENT_BOX(EventBox[i][j]),TRUE);
            /*On regarde la case dans le jeu, et on lui associe une image*/
            p = g.grid[j][i];
            if(empty_pile(p))
            {
                image = get_image_square('Z');
            }
            else
            {
                image = get_image_square(peak_pile(p).field);           
            }
            /*On place dans l'EventBox l'image, puis dans la grille l'EventBox*/
            gtk_container_add(GTK_CONTAINER(EventBox[i][j]),image);
            gtk_grid_attach (GTK_GRID (grid_table), EventBox[i][j], i, j, 1, 1);
        }
    }
    /*On connecte ensuite les signaux avec les différentes EventBox*/
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            user_data[i].main = grid_table;
            user_data[i].child = EventBox[i][j];
            user_data[i].x = i;
            /*Ajout du signal vers la fonction position_tile, qui permet l'intéraction via la souris avec la grille*/
            g_signal_connect ((gpointer)EventBox[i][j], "button_press_event",
                        G_CALLBACK(position_tile),&(user_data[i]));
       }
    }
    /*Creation de l'overlay de previsualisation*/
    previsu = gtk_overlay_new();/*Creation de l'overlay*/
    fixe = gtk_fixed_new();/*Creation du container contenant l'overlay, qui permet de placer de widgets au pixel près*/
    gtk_container_add(GTK_CONTAINER(pFrame_grid),previsu);
    gtk_container_add(GTK_CONTAINER(previsu),grid_table);
    gtk_overlay_add_overlay(GTK_OVERLAY(previsu),fixe);
    gtk_overlay_set_overlay_pass_through (GTK_OVERLAY (previsu), fixe, TRUE);
    gtk_box_pack_start(GTK_BOX(hbox1), pFrame_grid, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox1), pFrame_selec, TRUE, TRUE, 0);

    /*Creation de la Fenetre d'action*/

    /*Creation de la box du menu*/
    vbox_menu1 = gtk_box_new(GTK_ORIENTATION_VERTICAL,3);

    /*Creation du sous-menu lors du mode sélection*/
    vbox_menu_posage = gtk_box_new(GTK_ORIENTATION_VERTICAL,3);

    /*Creations des boutons du sous-menus*/
    bouton_pose = (GtkWidget*) gtk_button_new_with_label("Poser");
    bouton_rotate = (GtkWidget*) gtk_button_new_with_label("Tourner");
    gtk_box_pack_start(GTK_BOX(vbox_menu_posage), bouton_pose,FALSE,FALSE,2);
    gtk_container_set_border_width(GTK_CONTAINER(bouton_pose),10);
    gtk_box_pack_start(GTK_BOX(vbox_menu_posage), bouton_rotate,FALSE,FALSE,2);
    gtk_container_set_border_width(GTK_CONTAINER(bouton_rotate),10);



    /*Creation du bouton pour passer en mode sélection*/
    bouton1 = (GtkWidget*) gtk_toggle_button_new_with_label("Poser une tuile");
    gtk_box_pack_start(GTK_BOX(vbox_menu1), bouton1,FALSE,FALSE,2);
    gtk_container_set_border_width(GTK_CONTAINER(bouton1),30);

        gtk_box_pack_start(GTK_BOX(vbox_menu1),vbox_menu_posage,FALSE,FALSE,2);
    /*Creation du bouton de conseil*/
    bouton2 = (GtkWidget*) gtk_button_new_with_label("Conseil");
    gtk_container_set_border_width(GTK_CONTAINER(bouton2),30);
    gtk_box_pack_start(GTK_BOX(vbox_menu1), bouton2,FALSE,FALSE,2);
    /*Creation du bouton d'affichage des règles*/
    bouton_rules = (GtkWidget*) gtk_button_new_with_label("Afficher les règles");
    gtk_container_set_border_width(GTK_CONTAINER(bouton_rules),30);
    gtk_box_pack_start(GTK_BOX(vbox_menu1), bouton_rules,FALSE,FALSE,2);
    /*Ajout des différents widgets*/

    gtk_container_add(GTK_CONTAINER(pFrame_selec),vbox_menu1);
    /*Ajout des signaux*/
    g_signal_connect ((gpointer) bouton1, "toggled", G_CALLBACK (menu_selection),NULL);
    g_signal_connect ((gpointer) bouton2, "clicked", G_CALLBACK (advice), NULL);
    g_signal_connect ((gpointer) bouton_rotate, "clicked", G_CALLBACK (rotation_tile),NULL);
    g_signal_connect ((gpointer) bouton_pose, "clicked", G_CALLBACK (add_tile),NULL);
    g_signal_connect ((gpointer) bouton_rules, "clicked", G_CALLBACK (gprint_rules),NULL);


    /*Creation de l'affichage de la main*/
    pFrame_main = gtk_frame_new("Votre Main:");
    gtk_frame_set_shadow_type (GTK_FRAME (pFrame_main), GTK_SHADOW_IN);
    gtk_box_pack_start(GTK_BOX(vbox1), pFrame_main, FALSE, FALSE, 0);
    /*Creation de la grille pour les tuiles de la main*/
    hand_table = gtk_grid_new();
    gtk_grid_set_column_spacing (GTK_GRID(hand_table),20);
    gtk_container_add(GTK_CONTAINER(pFrame_main), hand_table);
    gtk_container_set_border_width (GTK_CONTAINER(hand_table),20);
    /*On parcourt la main*/
    for(i=0;i<g.hd_lg;i++)
    {
        /*On ajoute la tuile à la grille*/
        gtile = gprint_tile(display_list(g.hand,i+1));
        gtk_grid_attach (GTK_GRID (hand_table), gtile, i, 0, 1, 1);
        /*On ajoute éventuellement une barre verticale*/
        if(i != g.hd_lg-1)
            {
                pSeparator = gtk_separator_new(GTK_ORIENTATION_VERTICAL);
                gtk_grid_attach (GTK_GRID (hand_table), pSeparator, i, 0, 2, 1);
            }
        /*Ajout des boutons de sélection de tuile*/
        char label[3];
        sprintf(label,"%d",i+1);
        bouton_main[i] = (GtkWidget*) gtk_button_new_with_label(label);
        gtk_grid_attach(GTK_GRID(hand_table),bouton_main[i],i,4,1,1);
        g_signal_connect((gpointer)bouton_main[i],"clicked",G_CALLBACK(selection_tile),&(id_tiles[i]));
    }

    /* Affichage de la fenetre */
    gtk_widget_show_all(pWindow);
    gtk_widget_hide(vbox_menu_posage);/*On cache initialement le sous-menu de sélection*/
    /* Démarrage de la boucle événementielle */
    gtk_main(); 
    free_grid(g.grid,g.lg);
    free_list(g.hand);
    free_list(g.posed);
    return EXIT_SUCCESS; 
}

/*A partir d'une caractère, renvoie l'image du terrain associé, ou bien une image par défaut*/
GtkWidget* get_image_square(char c)
{
	switch(c)
	{
		case('V'):
			return gtk_image_new_from_file("./image/ville.png");
			break;
		case('L'):
			return gtk_image_new_from_file("./image/lac.png");
			break;
		case('P'):
			return gtk_image_new_from_file("./image/plaine.png");
			break;
		case('R'):
			return gtk_image_new_from_file("./image/ressource.png");
			break;
		case('U'):
			return gtk_image_new_from_file("./image/usine.png");
			break;
		case('F'):
			return gtk_image_new_from_file("./image/foret.png");
			break;
		default:
			return gtk_image_new_from_file("./image/default.png");
			break;
	}
}

/*Fonction de création du widget d'une tuile pour l'affichage de la main*/
GtkWidget* gprint_tile(tile t)
{
    GtkWidget *tile_table;/*Creation d'une grille pour la tuile*/
    GtkWidget* widget;/*Widget temporaire pour stocker chaque case*/
    tile_table = gtk_grid_new();
    int i,j;
    /*Pour chaque case, on attache son widget a la grille*/
    for(i = 0; i < 3; i++)
    {
        for(j=0 ; j<2 ; j++)
        {
            widget = get_image_square(t.fields[2*i+j]);
            gtk_grid_attach (GTK_GRID (tile_table), widget, j, i, 1, 1);
            gtk_widget_show(widget);
        }

    }
    return tile_table;
}

/*Affichage de la tuile de l'overlay*/
GtkWidget *gprint_tile_overlay()
{
    tile t = selected_tile;
    GtkWidget *tile_table;
    GtkWidget* widget;
    tile_table = gtk_grid_new();
    int i,j;
    /*A chaque rotation son affichage*/
    switch(t.rotation)
    {
        case 0:
            for(i = 0; i < 3; i++)
            {
                for(j=0 ; j<2 ; j++)
                {
                    widget = get_image_square(t.fields[2*i+j]);
                    gtk_grid_attach (GTK_GRID (tile_table), widget, j, i, 1, 1);
                    gtk_widget_show(widget);
                }
            }
            break;
        case 1:
            for(i = 0; i < 2; i++)
            {
                for(j=0 ; j<3 ; j++)
                {
                    widget = get_image_square(t.fields[2*(2-j)+i]);
                    gtk_grid_attach (GTK_GRID (tile_table), widget, j, i, 1, 1);
                    gtk_widget_show(widget);
                }
            }
            break;
        case 2:
            for(i = 0; i < 3; i++)
            {
                for(j=0 ; j<2 ; j++)
                {
                    widget = get_image_square(t.fields[2*(2-i)+1-j]);
                    gtk_grid_attach (GTK_GRID (tile_table), widget, j, i, 1, 1);
                    gtk_widget_show(widget);
                }
            }
            break;
        case 3:
            for(i = 0; i < 2; i++)
            {
                for(j=0 ; j<3 ; j++)
                {
                    widget = get_image_square(t.fields[2*j+1-i]);
                    gtk_grid_attach (GTK_GRID (tile_table), widget, j, i, 1, 1);
                    gtk_widget_show(widget);
                }
            }
            break;
    }
    return tile_table;

}

/*Fonction de mise a jour d'une case*/
void update_case(game g, int x, int y)
{
    GtkWidget *box = gtk_grid_get_child_at(GTK_GRID(grid_table),y,x);/*On recupere l'EventBox de la case concernée*/
    GtkWidget *child = gtk_bin_get_child(GTK_BIN(box));/*On recupere l'image de la case*/
    GtkWidget *new_child;
    gtk_widget_destroy(child);/*On détruit l'ancienne image*/
    pile p = g.grid[x][y];
    /*On met a jour l'image, en vérifiant que la case n'est pas vide*/
    if(empty_pile(p))
    {
        new_child = get_image_square('Z');
    }
    else
    {
        new_child = get_image_square(peak_pile(p).field);           
    }
     gtk_container_add(GTK_CONTAINER(box),new_child);  
    gtk_widget_show(new_child); 
}

/*Fonction d'ajout d'une tuile*/
void add_tile(GtkWidget *button, gpointer user_data)
{
    int i;
    int rules = 0;/*Accumulateur pour savoir si les regles sont respectées*/
    int x = selected_x;/*Coordonnées de la tuile à poser*/
    int y = selected_y;
    int rota = selected_tile.rotation;/*Rotation de la tuile*/
    int position[2];
    position[0] = selected_x;
    position[1] = selected_y;
    if (can_insert(rota,g.lg,position) != 1)/*On verifie qu'on est dans les limites de la grille*/
    {
        rules = 0;
    }
    /*Si c'est le cas, on vérifie qu'on respecte les différentes régles*/
    else
    {
        rules = covering_tile_test(g,selected_tile,position)*covered_lake(g.grid,position,rota);
        for(i=0;i<length_list(g.posed);i++)
            {
              rules = rules * tile_covered_test(g,display_list(g.posed,i),position,rota);
            }       
    }
    /*Si toutes les regles sont respectées*/
    if(rules != 0)
        {
            /*On l'insere*/
            insert_tile(g.grid,g.lg,&selected_tile,position);
            /*On met a jour les différentes cases concernées*/
            update_case(g,x,y);
            update_case(g,x+1,y);
            update_case(g,x,y+1);
            update_case(g,x+1,y+1);  
            if(rota%2 == 0) 
            { 

                update_case(g,x+2,y);
                update_case(g,x+2,y+1);
            }
            else
            {
                update_case(g,x,y+2);
                update_case(g,x+1,y+2);        
            }
            /*On met a jour les listes*/
            push_list(&(g.posed),selected_tile);
            remove_list(&(g.hand),id_tile_selected+1);
            g.hd_lg -= 1;
            /*On met a jour l'affichage de la liste, en détruisant et reconstruisant l'ensemble de la grille de la main*/
            GtkWidget *hand_table = gtk_grid_new();
            GtkWidget *widget, *pSeparator;
            int i;
            gtk_grid_set_column_spacing (GTK_GRID(hand_table),20);
            GtkWidget *old = gtk_bin_get_child(GTK_BIN(pFrame_main));
            gtk_widget_destroy(old);
            gtk_container_add(GTK_CONTAINER(pFrame_main), hand_table);
            gtk_container_set_border_width (GTK_CONTAINER(hand_table),20);
            for(i=0;i<g.hd_lg;i++)
            {
                widget = gprint_tile(display_list(g.hand,i+1));
                gtk_grid_attach (GTK_GRID (hand_table), widget, i, 0, 1, 1);
                if(i != g.hd_lg-1)
                    {
                        pSeparator = gtk_separator_new(GTK_ORIENTATION_VERTICAL);
                        gtk_grid_attach (GTK_GRID (hand_table), pSeparator, i, 0, 2, 1);
                    }
                char label[3];
                sprintf(label,"%d",i+1);
                bouton_main[i] = (GtkWidget*) gtk_button_new_with_label(label);
                gtk_grid_attach(GTK_GRID(hand_table),bouton_main[i],i,4,1,1);
                g_signal_connect((gpointer)bouton_main[i],"clicked",G_CALLBACK(selection_tile),&(id_tiles[i]));
            }
            /*Si il y avait un overlay, il est viré*/
            if(overlay == 1)
            {
                gtk_widget_destroy(Overlay_contour);
            }
            overlay = 0;
            gtk_widget_show_all(hand_table);
            /*Test de fin de partie*/
            if(g.hd_lg == 0)
            {
                print_end();
            }
            else
            {
                /*Par défaut, on selectionne la tuile 1 de la liste*/
                selected_tile = display_list(g.hand,0);
                id_tile_selected = 0;
            }
        }
    else
        /*Si les regles ne sont pas respectées, on affiche une fenêtre pour le monter*/
        {
            GtkWidget *not_possible_window;
            GtkWidget *text_np;
            GtkWidget * np_box;
            GtkWidget *np_exit;
            /*Creation de la fenetre*/
            not_possible_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
            np_box = gtk_box_new(GTK_ORIENTATION_VERTICAL,100);
            gtk_container_add(GTK_CONTAINER(not_possible_window),np_box);
            gtk_window_set_title(GTK_WINDOW(not_possible_window),"Impossible de poser");
            gtk_window_set_position(GTK_WINDOW(not_possible_window),GTK_WIN_POS_CENTER);
            gtk_container_set_border_width(GTK_CONTAINER(not_possible_window), 30);
            /*Ajout d'un texte*/
            text_np = gtk_label_new("Impossible de poser une tuile ici !\nRegardez les régles !");
            gtk_container_add(GTK_CONTAINER(np_box),text_np);
            /*Ajout d'un bouton retour*/
            np_exit = gtk_button_new_with_label("Revenir");
            g_signal_connect((gpointer)np_exit,"clicked",G_CALLBACK(destroy_window),not_possible_window);
            gtk_container_add(GTK_CONTAINER(np_box),np_exit);
            gtk_widget_show_all(not_possible_window);
        }
}

/*Fonction d'affichage de la previsu en cliquant sur la grille*/
void position_tile(GtkWidget *widget, gpointer user_data)
{
    /*Si on est en mode sélection*/
    if(is_selecting == 1)
    {
        GValue i = G_VALUE_INIT;
        g_value_init (&i, G_TYPE_INT);
        GValue j = G_VALUE_INIT;
        g_value_init(&j, G_TYPE_INT);
        gtk_container_child_get_property(GTK_CONTAINER(grid_table),widget,"left-attach",&i);/*On recupere les coordonnées de la case sur laquelle on a cliqué*/
        gtk_container_child_get_property(GTK_CONTAINER(grid_table),widget,"top-attach",&j);
        if(overlay == 1)/*Si il y'a deja une previsu, on la détruit*/
            {
                gtk_widget_destroy(Overlay_contour);
            }
        Overlay_contour =gprint_tile_overlay(selected_tile);
        /*On affiche la previsu*/
        gtk_fixed_put(GTK_FIXED(fixe),Overlay_contour,g_value_get_int(&i)*32,g_value_get_int(&j)*32);
        overlay = 1;
        /*On stock les valeurs des coordonnéees*/
        selected_x = g_value_get_int(&j);
        selected_y = g_value_get_int(&i);
        gtk_widget_show(Overlay_contour);
    }
}

/*Fonction pour passer en mode 'Pose' ou non*/
void menu_selection(GtkWidget *widget, gpointer user_data)
{
    /*Si on est en mode sélection, on cache la fenêtre sélection et détruit la prévisualisation de la tuile, si elle existe*/
    if(is_selecting == 1)
    {
        is_selecting = 0;
        gtk_widget_hide(vbox_menu_posage);
        if(overlay == 1)
        {
            gtk_widget_destroy(Overlay_contour);
        }
        overlay = 0;
    }
    /*Sinon, on affiche le menu de sélection*/
    else
    {
        is_selecting = 1;
        gtk_widget_show(vbox_menu_posage);
    }
}

/*Fonction de selection d'une tuile grâce aux boutons numérotés*/
void selection_tile(GtkWidget *widget, gpointer user_data)
{
    /*Si on est en mode séléction, on stock la tuile dans la variable globale selected_tile*/
    if(is_selecting == 1)
    {
        int *n = user_data;
        id_tile_selected = *n; 
        selected_tile = display_list(g.hand,*n+1);
    }
}

/*Fonction de rotation d'une tuile grâce au bouton*/
void rotation_tile(GtkWidget *widget, gpointer user_data)
{
    /*On fait tourner la tuile séléctionnée*/
    selected_tile.rotation = (selected_tile.rotation + 1) % 4;
    /*Si elle était affiché, on la détruit et la ré-affiche avec la nouvelle rotation*/
    if(overlay == 1)
    {
        gtk_widget_destroy(Overlay_contour);
    }
    overlay = 1;
    Overlay_contour = gprint_tile_overlay(selected_tile);
    gtk_fixed_put(GTK_FIXED(fixe),Overlay_contour,selected_y*32,selected_x*32);
    gtk_widget_show(Overlay_contour);
}

/*Fonction d'affichage de la fin de partie*/
void print_end()
{
    /*Création des widgets de la fenêtre*/
    GtkWidget *end_window;
    GtkWidget *end_box;
    GtkWidget *text;
    GtkWidget *bouton_exit;
    end_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(end_window),"Félicitations !");
    gtk_window_set_default_size(GTK_WINDOW(end_window),400,400);
    gtk_window_set_position(GTK_WINDOW(end_window),GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(end_window), 5);
    end_box = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_container_add(GTK_CONTAINER(end_window),end_box);
    /*Remplissage de la fenêtre*/
    char score_txt[150];
    int* detail = (int*)calloc(4,sizeof(int));
    int* rules = (int*)calloc(5,sizeof(int));
    int score_total = score(g.grid,g.lg,rules,detail);
    sprintf(score_txt,"Félicitations, vous avez terminé la partie !\n\nVoici votre score: %d\nDetail du score :\n\tForêts : %d\n\tVillages : %d\n\tRessources : %d\n\t Lacs : %d\n",score_total,detail[0],detail[3],detail[2],detail[1]); 
    text = gtk_label_new(score_txt);
    gtk_container_add(GTK_CONTAINER(end_box),text);
    bouton_exit = gtk_button_new_with_label("Quitter");
    g_signal_connect((gpointer)bouton_exit, "clicked", G_CALLBACK(gtk_main_quit), NULL);
    gtk_container_add(GTK_CONTAINER(end_box),bouton_exit);
    gtk_widget_show_all(end_window);
}

/*Fonction de destruction d'une window*/
void destroy_window(GtkWidget *widget, gpointer user_data)
{
    GtkWidget *window = user_data;
    gtk_widget_destroy(window);
}

/*Fonction d'affichage des conseils*/
void advice(GtkWidget *widget, gpointer user_data)
{
    /*Création des widgets de la fenêtre*/
    GtkWidget *advice_window;
    GtkWidget *advice_box;
    GtkWidget *text;
    GtkWidget *bouton_exit;
    advice_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(advice_window),"Besoin d'aide ?");
    gtk_window_set_default_size(GTK_WINDOW(advice_window),200,100);
    gtk_window_set_position(GTK_WINDOW(advice_window),GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(advice_window), 5);
    advice_box = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_container_add(GTK_CONTAINER(advice_window),advice_box);
    /*Remplissage de la fenêtre*/
    char advice_txt[150];
    int id_res,score_res,x_res,y_res,rota_res;
    int* detail = (int*)calloc(4,sizeof(int));
    best_tile(&g,&id_res,&score_res,&x_res,&y_res,&rota_res,detail);
    sprintf(advice_txt,"Besoin d'aide ?\n\t Poser la tuile %d en %d %d avec une rotation %d\n",id_res,x_res+1,y_res+1,rota_res); 
    text = gtk_label_new(advice_txt);
    gtk_container_add(GTK_CONTAINER(advice_box),text);
    bouton_exit = gtk_button_new_with_label("Retour");
    g_signal_connect((gpointer)bouton_exit, "clicked", G_CALLBACK(destroy_window), advice_window);
    gtk_container_add(GTK_CONTAINER(advice_box),bouton_exit);
    gtk_widget_show_all(advice_window);
}


/*Fonction d'affichage des regles*/
void gprint_rules(GtkWidget *widget, gpointer user_data)
{
    /*Création des widgets de la fenêtre*/
    GtkWidget *rules_window;
    GtkWidget *rules_box;
    GtkWidget *text;
    GtkWidget *bouton_exit;
    rules_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(rules_window),"The rules");
    gtk_window_set_default_size(GTK_WINDOW(rules_window),300,300);
    gtk_window_set_position(GTK_WINDOW(rules_window),GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(rules_window), 5);
    rules_box = gtk_box_new(GTK_ORIENTATION_VERTICAL,10);
    gtk_container_add(GTK_CONTAINER(rules_window),rules_box);
    /*Remplissage de la fenêtre*/
    text = gtk_label_new("Voici les règles du jeu : \n\nL'objectif est de poser l'intégralité des tuiles de la main, pour marquer le plus grand score possible.\n\nPour poser une tuile, il faut d'abord activer le mode 'Pose' en appuyant sur le bouton 'Poser une tuile'.\n\nIl faut ensuite séléctionner une tuile dans la main grâce aux boutons numérotés.\n\nVous pouvez ensuite cliquer sur la grille de jeu pour visualiser le résultat.\n\n Il est possible de tourner la tuile à  l'aide du bouton 'Tourner'.\n\nPour poser la tuile, il suffit alors d'appuyer sur 'Poser'.\n\nMais attention, on ne peut poser une tuile n'importe où.\n\n\t En effet, il faut que :\n\n\t\t -Aucune case lac ne doit être recouverte.\n\n\t\t -Aucune tuile n'est totalement recouverte. \n\n\t\t -Une tuile doit reposer sur autre tuile.\n\n Ce sont les terrains visibles sur les cases qui rapportent des points :\n\n\t -Une case Forêt rapporte 2 points, \n\n\t -Une case Plaine rapporte 0 points, \n\n\t -Le plus grand village (plus grand bloc de cases Ville contigues) rapporte le nombre de case Ville de ce village points, \n\n\t -Chaque lac (ensemble de cases Lac contigues), il rapporte 3*(nombre de cases Lac du lac - 1) points, \n\n\t -Chaque couple Usine/Ressource rapporte 4 points, \n\n\n\t\t\t Bon jeu !");
    gtk_container_add(GTK_CONTAINER(rules_box),text);
    bouton_exit = gtk_button_new_with_label("Retour");
    g_signal_connect((gpointer)bouton_exit, "clicked", G_CALLBACK(destroy_window), rules_window);
    gtk_container_add(GTK_CONTAINER(rules_box),bouton_exit);
    gtk_widget_show_all(rules_window);    
}


