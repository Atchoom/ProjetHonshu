#include "init.h"
#include <stdio.h>
#include <stdlib.h>


char type_fields[6] = {'P','F','L','V','R','U'};

/* Fonction pour retourner un entier appartenant à [inf,sup[ */
int rand_int(int inf, int sup) {
	return (rand()%(sup-inf) + inf);
}

/* renvoie un tableau de nb_rule 0 ou 1 avec n valeurs à 1, rng si n<0 */
int*random_rule(int nb_rule, int n)
{
  if (n>nb_rule)
    {
      exit(1);
    }
  int k = 0; /* parcourir le tableau de règles */
  int count = 0; /* compter les règles */
  int*res = calloc(nb_rule,sizeof(int));
  int rng; /* choisir les n règles */
  if (n<0) /* si nombre de règles aléatoire */
    {
      for (k=0;k<nb_rule;k++) /* chaque règle est aléatoire */
	{
	  res[k] = rand_int(0,2);
	}
    }
  else /* sinon on a un nombre de règles */
    {
      /* boucle infinie si n>nb_rule */
      for (count=0;count<n;) /* tant qu'on a pas assez de règle */
	{
	  rng = rand_int(0,nb_rule); /* on tire une règle aléatoire */
	  if (res[rng] == 0) /* si elle était fausse on la met à vraie et count++ */
	    {
	      res[rng] = 1;
	      count++;
	    }
	}
    }
  return res;
}

/*Check si un entier est dans un tableau*/
int in_tab(int* t,int i, int size)
{
	int j;
	for(j=0;j<size;j++)
	{
		if(i == t[j])
		{
			return 1;
		}
	}
	return 0;
}

/* Fonction pour initialiser le jeu */
/*Doit retourner la grille et la liste des tuiles(pointeurs en argument)*/

game init_game (int n) {
	srand(time(NULL));
	int size = rand_int(7,MAX_SIZE); 
	int center = size/2;
	pile** playmap = create_playmap(size);
	list hand = create_list();
	list posed = create_list();
	int i;
	char start_fields[6];
	int field;
	int num_lac = 6;
	while (num_lac == 6) {
		num_lac = 0;
		for (i=0;i<6;i++) {
			field = rand_int(0,6); 
			if (field == 2) {
				num_lac ++;
			}
			start_fields[i]=type_fields[field];
		}
	}
	tile start_tile=create_tile(start_fields,0); 
	start_tile.position[0] = center;
	start_tile.position[1] = center;
	push_list(&posed,start_tile);
	insert_tile (playmap,size,&start_tile,start_tile.position); 
	tile curr_tile;
	int j;
	char tile_fields[6];
	for (j=0;j<12;j++) {
		for (i=0;i<6;i++) {
		field = rand_int(0,6); 
		tile_fields[i]=type_fields[field];
		}
		curr_tile=create_tile(tile_fields,j+1);
		push_list(&hand,curr_tile);
	}
	game result;
	result.rules = random_rule(5,n);
	result.lg = size;
	result.hd_lg = 12;
	result.grid = playmap;
	result.posed = posed;
	result.hand = hand;
	return result;
}


/*Fonction qui renvoie l'entier lu */

int next_int(FILE * f, char* addr, int* next)
{
	int count = 0;
	char res = ' ';
	char buff= 'z';
	while (buff != '\n' && buff != ' ')
	{
		if(fread(&buff,1,1,f) != 1)
			{
				fprintf(stderr,"next_int erreur de lecture : %s file\n",addr);
				return 1;
			}		
		count ++;
	}	
	fseek(f,-count,SEEK_CUR);
	if (fread(&res,1,count-1,f) != (unsigned int)(count-1))
		{
			fprintf(stderr,"next_int erreur de lecture : %s file\n",addr);
			return 1;
		}
	*next = atoi(&res);
	return 0;
}


/* Fonction creation grille à partir d'un fichier */

int init_game_f(game *g, char* addr_tile, char* addr_game)
{
	/*preparation du resultat*/
	game result;
	int lg,hd_lg;/*taille de la grille et taille de la main*/

	int* id_hand ; /*Liste des id des tuiles de la min*/

	int rules[5];

	list hand = create_list(); /*liste des tuiles de la main*/
	list posed = create_list(); /*Liste des tuiles posées*/
	list temp = create_list(); /*Liste de tuiles temporaire*/
	pile** grid; /*grille*/

	int nb_tile,i,j; /*Nombre de tuiles et deux itérateurs*/
	int id_start; /*Id de la tuile de départ*/

	/*buffers et autres*/
	char buffer[10];
	char curr_fields[6];
	int buff = 0;
	int id_curr = 0;/*identifiant de la tuile*/
	tile curr_tile;
	tile start_tile;
	/*Ouverture des fichiers*/
	FILE* file_tile = fopen(addr_tile,"r");
	FILE* file_game = fopen(addr_game,"r");
	if (file_tile == NULL)
	{
		fprintf(stderr,"grid_init_f erreur d'ouverture : %s file\n",addr_tile);
		return 1;
	}
	if (file_game == NULL)
	{
		fprintf(stderr,"grid_init_f erreur d'ouverture : %s file\n",addr_game);
		return 1;
	}
	/*Lecture du fichier game*/
	if(next_int(file_game,addr_game,&lg) == 1)
	{
		return 1;
	}
	fseek(file_game,1,SEEK_CUR);
	if (next_int(file_game,addr_game, &hd_lg) == 1)
	{
		return 1;
	}
	fseek(file_game,1,SEEK_CUR);
	id_hand = malloc(hd_lg*sizeof(int));
	for (i=0; i<hd_lg; i++)/*Main du joueur*/
	{
		if(next_int(file_game,addr_game,&buff)==1)
		{
			return 1;
		}
		id_hand[i] = buff;
		fseek(file_game,1,SEEK_CUR);		
	}
	if (next_int(file_game,addr_game,&id_start) == 1)
	{
		return 1;
	}
	/*Lecture fichier tile*/
	if(next_int(file_tile,addr_tile,&nb_tile)==1)
	{
		return 1;
	}
	fseek(file_tile,1,SEEK_CUR);	
	hand = create_list();
	posed = create_list();
	for (i=0; i<nb_tile; i++)
		{
			if(next_int(file_tile,addr_tile,&id_curr) == 1)
			{
				return 1;
			}
			fseek(file_tile,1,SEEK_CUR);
				for(j=0;j<6;j++)
				{
					if(fread(buffer,1,1,file_tile) != 1)
					{
						fprintf(stderr,"grid_init_f erreur de lecture : %s file\n",addr_tile);
						return 1;
					}
					curr_fields[j] = buffer[0];
					fseek(file_tile,1,SEEK_CUR);
				}
			curr_tile = create_tile(curr_fields,id_curr);
			push_list(&temp,curr_tile);

		}
	/*Les regles*/
	for (i=0;i<5;i++)
	{
		rules[i] = 0;
	}

	/*Creation de la main*/
	for (i=0; i<hd_lg;i++)
	{
		curr_tile = display_list(temp,nb_tile - id_hand[i]);
		push_list(&hand,curr_tile);
	}
	start_tile = display_list(temp,nb_tile - id_start);
	push_list(&posed,start_tile);		
	/*Creation de la grille et posage de la premiere tuile*/
	int position[2] = {(lg/2),(lg/2)};
	grid = create_playmap(lg);
	insert_tile(grid,lg,&start_tile,position);
	result.lg = lg;
	result.hd_lg = hd_lg;
	result.grid = grid;
	result.posed = posed;
	result.hand = hand;
	result.rules = rules;
	fclose(file_game);
	fclose(file_tile);
	free(id_hand);
	free_list(temp);
	*g = result;
	return 0;
}


/*Fonction génératrice de fichiers */
int create_files(int size,int nb_tiles,int* hand, int hd_size, int id_start, list tiles)
{
	tile curr_tile;
	int length = 0;
	int i,j;
	char buff[14*nb_tiles+3];
	/*Create file game*/
	length += sprintf(buff,"%d %d\n",size,hd_size);
	for (i=0;i<hd_size;i++)
	{
		length += sprintf(buff+length,"%d ",hand[i]);
	}
	length += sprintf(buff+length,"\n%d",id_start);
	FILE* file_game = fopen("file_game.txt","w+");
	if (file_game == NULL)
	{
		fprintf(stderr,"create_files erreur d'ouverture : %s file\n","file_game.txt");
		return 1;		
	}
	if(fwrite(buff,1,length,file_game) != (unsigned int)length)
	{
		fprintf(stderr,"create_files erreur d'écriture : %s file\n","file_game.txt");
		return 1;
	}
	fclose(file_game);
	/*Create file tile*/
	length = 0;
	length += sprintf(buff,"%d\n",nb_tiles);
	for(i=0;i<nb_tiles;i++)
	{
		curr_tile = remove_list(&tiles,nb_tiles-i);
		length += sprintf(buff+length,"%d\n",curr_tile.id);
		for (j=0;j<6;j = j+2)
		{	
			length += sprintf(buff+length,"%c %c\n",(curr_tile).fields[j],(curr_tile).fields[j+1]);
		}
	}
	FILE *file_tiles = fopen("file_tiles.txt","w+");
	if (file_tiles == NULL)
	{
		fprintf(stderr,"create_files erreur d'ouverture : %s file\n","file_tiles.txt");
		return 1;		
	}
	if(fwrite(buff,1,length,file_tiles) != (unsigned int)length)
	{
		fprintf(stderr,"create_files erreur d'écriture : %s file\n","file_game.txt");
		return 1;
	}
	fclose(file_tiles);
	return 0;
}

  /*Fonction de retrait de la derniere tuile*/
  int remove_last_tile(game* g)
  {
    int x,y;
    tile t = remove_list(&(g->posed),1);
    x=t.position[0];
    y=t.position[1];	
    if(t.rotation %2 ==1)
    {
      	pop_pile(&(g->grid[x][y]));
     	pop_pile(&(g->grid[x][y+1]));
      	pop_pile(&(g->grid[x][y+2]));
      	pop_pile(&(g->grid[x+1][y]));
      	pop_pile(&(g->grid[x+1][y+1]));
      	pop_pile(&(g->grid[x+1][y+2]));
    }
    else
    {
      	pop_pile(&(g->grid[x][y]));
      	pop_pile(&(g->grid[x+1][y]));
      	pop_pile(&(g->grid[x+2][y]));
      	pop_pile(&(g->grid[x][y+1]));
      	pop_pile(&(g->grid[x+1][y+1]));
      	pop_pile(&(g->grid[x+2][y+1]));
    }
  return 1;
  }

game init_game2 (int n, int size) {
	srand(time(NULL));
	int center = size/2;
	pile** playmap = create_playmap(size);
	list hand = create_list();
	list posed = create_list();
	int i;
	char start_fields[6];
	int field;
	int num_lac = 6;
	while (num_lac == 6) {
		num_lac = 0;
		for (i=0;i<6;i++) {
			field = rand_int(0,6); 
			if (field == 2) {
				num_lac ++;
			}
			start_fields[i]=type_fields[field];
		}
	}
	tile start_tile=create_tile(start_fields,0); 
	start_tile.position[0] = center;
	start_tile.position[1] = center;
	push_list(&posed,start_tile);
	insert_tile (playmap,size,&start_tile,start_tile.position); 
	tile curr_tile;
	int j;
	char tile_fields[6];
	for (j=0;j<12;j++) {
		for (i=0;i<6;i++) {
		field = rand_int(0,6); 
		tile_fields[i]=type_fields[field];
		}
		curr_tile=create_tile(tile_fields,j+1);
		push_list(&hand,curr_tile);
	}
	game result;
	result.rules = random_rule(5,n);
	result.lg = size;
	result.hd_lg = 12;
	result.grid = playmap;
	result.posed = posed;
	result.hand = hand;
	return result;
}


