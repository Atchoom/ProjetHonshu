#include <stdio.h>
#include "recouvrement.h"
#include "list.h"

int zone_insert(int x, int y, int position[2],int rota)
{
	int res = 1;
	switch(rota%2)
	{
		case 0:
			res = res * ((x-position[0] >= 0) && (x-position[0] <= 2));
			res = res * ((y-position[1] >= 0) && (y-position[1] <= 1));
			break;
		case 1:
			res = res * ((x-position[0] >= 0) && (x-position[0] <= 1));
			res = res * ((y-position[1] >= 0) && (y-position[1] <= 2));
			break;
	}
	return 1-res;
}

int tile_covered_test(game g, tile t, int position[2], int rota)
{
  if (t.position[0] == -1) /* si la tuile n'est pas encore placée */
    {
      return 1;
    }
  else
    {
      int t0,t1,t2,t3,t4,t5; /* stockage des 6 tests */
      if (t.rotation % 2 == 0) /* si la tuile est posée verticalement */
	{
	  t0 = ((peak_pile(g.grid[t.position[0]][t.position[1]])).tile == t.id) && (zone_insert(t.position[0],t.position[1],position,rota)); /* test si le terrain en haut à gauche de la tuile est à découvert */
	  t1 = ((peak_pile(g.grid[t.position[0]][t.position[1]+1])).tile == t.id) && (zone_insert(t.position[0],t.position[1]+1,position,rota)); 
	  t2 = ((peak_pile(g.grid[t.position[0]+1][t.position[1]])).tile == t.id) && (zone_insert(t.position[0]+1,t.position[1],position,rota));
	  t3 = ((peak_pile(g.grid[t.position[0]+1][t.position[1]+1])).tile == t.id) && (zone_insert(t.position[0]+1,t.position[1]+1,position,rota));
	  t4 = ((peak_pile(g.grid[t.position[0]+2][t.position[1]])).tile == t.id)&& (zone_insert(t.position[0]+2,t.position[1],position,rota));
	  t5 = ((peak_pile(g.grid[t.position[0]+2][t.position[1]+1])).tile == t.id)&& (zone_insert(t.position[0]+2,t.position[1]+1,position,rota));
	  if (t0 || t1 || t2 || t3 || t4 || t5) /* si un terrain au moins est à découvert */
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }
	}
      else /* sinon elle est posée horizontalement */
	{
	  t0 = ((peak_pile(g.grid[t.position[0]][t.position[1]])).tile == t.id)&& (zone_insert(t.position[0],t.position[1],position,rota));
	  t1 = ((peak_pile(g.grid[t.position[0]][t.position[1]+1])).tile == t.id)&& (zone_insert(t.position[0],t.position[1]+1,position,rota));
	  t2 = ((peak_pile(g.grid[t.position[0]][t.position[1]+2])).tile == t.id)&& (zone_insert(t.position[0],t.position[1]+2,position,rota));
	  t3 = ((peak_pile(g.grid[t.position[0]+1][t.position[1]])).tile == t.id)&& (zone_insert(t.position[0]+1,t.position[1],position,rota));
	  t4 = ((peak_pile(g.grid[t.position[0]+1][t.position[1]+1])).tile == t.id)&& (zone_insert(t.position[0]+1,t.position[1]+1,position,rota));
	  t5 = ((peak_pile(g.grid[t.position[0]+1][t.position[1]+2])).tile == t.id)&& (zone_insert(t.position[0]+1,t.position[1]+2,position,rota));
	  if (t0 || t1 || t2 || t3 || t4 || t5)
	    {
	      return 1;
	    }
	  else
	    {
	      return 0;
	    }
	}
    }
}

tuple* square_covered(game game,int*field_nb) {
	int max_size = (game.lg)*(game.lg);
	tuple* square_covered = calloc(max_size,sizeof(tuple));
	int i,j;
	int acc = 0;
	for (i=0;i<game.lg;i++) {
		for (j=0;j<game.lg;j++) {
			if (game.grid[i][j] != NULL) {
				tuple pos;
				pos.int1 = i;
				pos.int2 = j;
				square_covered[acc] = pos;
				acc ++ ;
			}
		}
	}
	tuple* result = malloc(acc*sizeof(tuple));
	int k;
	for (k=0;k<acc;k++) {
		result[k] = square_covered[k];
	}
	free(square_covered);
	*field_nb = acc;
	return result;
}
int covering_tile_test(game g, tile t, int position[2])
{
  int t0,t1,t2,t3,t4,t5;
  if (can_insert(t.rotation,g.lg,position) && t.rotation%2 == 0)
    {
      t0 = empty_pile(g.grid[position[0]][position[1]]);/* ici on teste si chaque case est vide, si c'est vrai alors le test est faux */
      t1 = empty_pile(g.grid[position[0]][position[1]+1]);
      t2 = empty_pile(g.grid[position[0]+1][position[1]]);
      t3 = empty_pile(g.grid[position[0]+1][position[1]+1]);
      t4 = empty_pile(g.grid[position[0]+2][position[1]]);
      t5 = empty_pile(g.grid[position[0]+2][position[1]+1]);
      if (t0 && t1 && t2 && t3 && t4 && t5) /* la tuile ne recouvre rien */
	{
	  return 0;
	}
      else
	{
	  return 1;
	}
    }
  else if ((t.rotation % 2 == 1) && can_insert(t.rotation,g.lg,position))
    {
      t0 = empty_pile(g.grid[position[0]][position[1]]);
      t1 = empty_pile(g.grid[position[0]][position[1]+1]);
      t2 = empty_pile(g.grid[position[0]][position[1]+2]);
      t3 = empty_pile(g.grid[position[0]+1][position[1]]);
      t4 = empty_pile(g.grid[position[0]+1][position[1]+1]);
      t5 = empty_pile(g.grid[position[0]+1][position[1]+2]);
      if (t0 && t1 && t2 && t3 && t4 && t5)
	{
	  return 0;
	}
      else
	{
	  return 1;
	}
    }
  return 0;
}

int** covered_squares (tile t, int position[2]){
	int i;
	int** res=malloc(6*sizeof(int*));
	for (i = 0;i<6;i++){
	    res[i]=malloc(2*sizeof(int));
	}
	if (t.rotation%2==1){ /* La tuile est dans le sens vertical*/
		for (i=0;i<6;i++){
			res[i][0]=position[0]+i/3;
			res[i][1]=position[1]+i%3;
		}
	}
	else{ /* La tuile est dans le sens horizontal */
		for (i=0;i<6;i++){
			res[i][0]=position[0]+i%3;
			res[i][1]=position[1]+i/3;
		}
	}
	return res;
}

int covered_lake(pile** grid, int* position, int orientation){
	int i;
	if (orientation%2==1){/* La tuile est dans le sens vertical */
		for (i=0; i<6; i++){
			if(!empty_pile(grid[position[0]+i/3][position[1]+i%3]))
			{
				if ((grid[position[0]+i/3][position[1]+i%3]->data).field=='L')
			    return 0;
		}
		}
	}
	else{
		for (i=0;i<6;i++){/* La tuile est dans le sens horizontal */
			if(!empty_pile(grid[position[0]+i%3][position[1]+i/3]))
			{

			if ((grid[position[0]+i%3][position[1]+i/3]->data).field=='L') 
			    return(0);
		}
		}
	}
	return(1);
}

int can_insert(int orientation, int size, int* position){
	int x = position[0];
	int y = position[1];
	if (orientation%2==0){ /* La tuile est dans le sens vertical */
		if (x+3>size || y+2>size)
			return 0; /*Insertion impossible*/
		return 1; /*Pas de soucis*/
	}
	if (x+2>size || y+3>size) /* La tuile est dans le sens horizontal */
		return 0; /*Insertion impossible*/
	return 1; /*Pas de soucis*/
}

int game_over(game game) {
  return (length_list(game.hand) == 0);
}
