#ifndef RECOUVREMENT_H
#define RECOUVREMENT_H
#include "pile.h"
#include "init.h"

/**
 * \file recouvrement.h
 * \brief Gère les tests de recouvrements
 * \author ByeZoé
 * \date vendredi 30 mars
 *
 * Ce fichier décrit les différents tests de recouvrements pour respecter les règles du Honshu. Il contient 8 fonctions:
 * - \b zone_insert(x,y,position,rotation) pour tester si une case n'appartient pas à une tuile
 * - \b tile_covered_test(g,t,position,rota) pour tester si t a un terrain à découvert
 * - \b square_covered(game,fied_nb) pour renvoyer un tableau contenant tous les terrains recouverts par une tuile
 * - \b covering_tile_test(g,t,position) pour tester si une tuile repose sur une base
 * - \b covered_squares (t,position) pour renvoyer les terrains recouvert par une tuile si on la pose
 * - \b covered_lake(grid,position,orientation) pour tester si une tuile ne recouvre pas un lac si on la pose 
 * - \b can_insert(orientation,size,position) pour vérifier qu'une tuile rentre bien dans la grille
 * - \b game_over(game) pour tester si la partie est terminée
 */


/**
 * \brief Verifie si une case n'est pas dans la zone d'une tuile
 * \details Teste si les coordonnées (x,y) n'appartiennent pas a un case de la tuile posée en position avec une orientation rota.
 * \param x Coordonnée x de la case
 * \param y Coordonnées y de la case
 * \param position Coordonnées de la tuile concernée
 * \param rota Rotation de la tuile
 * \return 1 si elle n'est pas dans la zone, 0 sinon
 */ 
int zone_insert(int x, int y, int position[2], int rota);

/**
 * \brief Test le recouvrement total d'une tuile
 * \details Teste si la tuile passée en paramètre possèdera au moins un terrain à découvert après pose d'une tuile. La rotation et la position de la tuile doivent être à jour ou la fonction renverra une erreur. Si elle n'est pas placée elle est dans la main et donc elle n'est pas couverte. L'id de la tuile doit être unique.
 * \param g La partie contenant la grille
 * \param t La tuile à tester
 * \param position Les coordonnées de la tuile qui va être posée.
 * \param rota La rotation de la tuile qui va être posée.
 * \return Le booléen réponse au prédicat : t a un terrain à découvert
 * \attention La tuile doit être correctement placée ou dans la main.
 */
int tile_covered_test(game g, tile t, int position[2], int rota);

/**
 * \brief Test des terrains de la grille recouverts.
 * \details Test si la pile de la case est vide, et ajoute les positions des cases où ce n'est pas le cas dans le tableau. Stocke le nombre de tuples du tableau dans l'entier passé en paramètre.
 * \param game la partie considérée
 * \param field_nb Un pointeur valide vers un entier
 * \return une liste de tuples récapitulant les positions des cases de la grille recouverte
 */
tuple* square_covered(game game, int*field_nb);

/**
 * \brief Teste si la tuile recouvre partiellement une autre tuile en la posant.
 * \details Teste si la tuile passée en paramètre possède un support si on la place à position. La position doit être valide, sinon la fonction se termine sur une erreur.
 * \param g La partie considérée
 * \param t La tuile à tester
 * \param position L'emplacement désiré pour la tuile
 * \return true si la tuile a un support, false sinon
 * \attention position doit être valide pour l'insertion d'une tuile
 */
int covering_tile_test(game g, tile t, int position[2]);

/**
* \brief Liste des terrains recouverts par une tuile si on essaie de la poser sur le plateau
 * \details Liste les coordonnées des cases qui seront recouvertes
 * \param t la tuile à poser, position l'endroit où elle le sera
 * \param position La position à laquelle on veut poser la tuile
 * \return Une liste de doublets d'entiers représentant les coordonnées des cases qui seront recouvertes
*/
int** covered_squares (tile t, int position[2]);

/**
 * \brief Test de la règle Aucune case Lac ne peut être recouverte
 * \details Regarde si aucune case futurement recouverte par la tuile à poser n'est un lac
 * \param grid La grille de jeu
 * \param position Un pointeur valide vers un couple d'entier désigneant l'endroit d'insertion
 * \param orientation Un pointeur valide vers un entier désigneant le sens de la tuile
 * \return 1 si la règle est respectée, 0 sinon
*/
int covered_lake(pile** grid, int* position, int orientation);

/**
 * \brief Vérifie que les conditions d'insertion de la tuile sont valides
 * \details Vérifie que la position d'insertion et l'orientation d'une tuile ne la feraient pas dépasser de la grille de jeu
 * \param orientation L'orientation de la tuile
 * \param size La taille de la grille
 * \param position l'endroit où on veut insérer le coin supérieur gauche de la tuile
 * \return true si la tuile ne dépasse pas, false sinon
 */
int can_insert(int orientation, int size, int* position);

/**
 * \brief Test de fin de partie
 * \details Teste s'il reste des tuiles à poser dans la main du joueur
 * \param game la partie considérée
 * \return 1 si la game est terminée, 0 sinon
 */
int game_over(game game);

#endif
