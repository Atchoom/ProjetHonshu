  #include <stdlib.h>
#include <stdio.h>
#include "grid.h"



/* Fonction création de grille de jeu */

pile** create_playmap (int size) {
	pile** playmap = (pile**)malloc(size*sizeof(pile*)) ;
	int j,i;
	for (j=0;j<size;j++) {
		playmap[j] = (pile*)malloc(size*sizeof(pile)) ;
    for(i=0;i<size;i++)
    {
      playmap[j][i] = create_pile();
    }
	}
	return playmap ;
}

/* Fonction de désallocation d'une grille */

void free_grid (pile **grid, int size)
{
  int i,j;
  for(i=0;i<size; i++)
  {
    for(j=0;j<size;j++)
    {
      free_pile(grid[i][j]);
    }
    free(grid[i]);
  }
  free(grid);
}


/* Fonction création d'une tuile */

tile create_tile (char fields[6], int id) {
	tile newTile;
	newTile.position[0] = -1;
	newTile.position[1] = -1;
	newTile.rotation = 0;
  newTile.id = id;
	int i = 0;
	for (i=0;i<6;i++) {
		newTile.fields[i] = fields[i];
	}
	return newTile;
}

/* Fonction rotation d'une tuile */

void rotate(tile*tile){
	tile->rotation=(tile->rotation+1)%4;
}

/*Fonction affichage d'une tuile*/
void print_tile(tile t)
{
  /*printf("%d\n",t.id);*/
  switch (t.rotation)
    {
    case 0:
      printf("%c%c\n%c%c\n%c%c\n",t.fields[0],t.fields[1],t.fields[2],t.fields[3],t.fields[4],t.fields[5]);
      break;
    case 1:
      printf("%c%c%c\n%c%c%c\n\n",t.fields[4],t.fields[2],t.fields[0],t.fields[5],t.fields[3],t.fields[1]);
      break;
    case 2:
      printf("%c%c\n%c%c\n%c%c\n",t.fields[5],t.fields[4],t.fields[3],t.fields[2],t.fields[1],t.fields[0]);
      break;
    case 3:
      printf("%c%c%c\n%c%c%c\n\n",t.fields[1],t.fields[3],t.fields[5],t.fields[0],t.fields[2],t.fields[4]);
      break;
    }
}

element create_element(tile tile,int num) {
  element newElement;
  newElement.tile = tile.id;
  newElement.field=tile.fields[num];
  return newElement;
}

/* Fonction d'insertion d'une tuile dans la grille */

int insert_tile(pile** playmap,int size,tile *ptile,int position[2]) {
  if (position[0] > size  || position[1] > size) {
	  fprintf(stderr,"insertion impossible, position out of playmap\n");
	  return 1;
  }
  int i;
  element elt_tile[6];
  for (i=0;i<6;i++) {
    elt_tile[i] = create_element(*ptile,i);
  }
  if (ptile->rotation == 0) {
	  if ((position[0]+3) > size  || (position[1]+2) > size) {
	  fprintf(stderr,"insertion impossible, position out of playmap\n");
	  return 1;
	  }  
    push_pile(&playmap[position[0]][position[1]],elt_tile[0]);
    push_pile(&playmap[position[0]][position[1]+1],elt_tile[1]);
    push_pile(&playmap[position[0]+1][position[1]],elt_tile[2]);
    push_pile(&playmap[position[0]+1][position[1]+1],elt_tile[3]);
    push_pile(&playmap[position[0]+2][position[1]],elt_tile[4]);
    push_pile(&playmap[position[0]+2][position[1]+1],elt_tile[5]);
    ptile->position[0]=position[0];
    ptile->position[1]=position[1];
    return 0;
  }
  else if (ptile->rotation == 1) {
	  if ((position[0]+2) > size  || (position[1]+3) > size) {
	  fprintf(stderr,"insertion impossible, position out of playmap\n");
	  return 1;
	  }
    push_pile(&playmap[position[0]][position[1]+2],elt_tile[0]);
    push_pile(&playmap[position[0]+1][position[1]+2],elt_tile[1]);
    push_pile(&playmap[position[0]][position[1]+1],elt_tile[2]);
    push_pile(&playmap[position[0]+1][position[1]+1],elt_tile[3]);
    push_pile(&playmap[position[0]][position[1]],elt_tile[4]);
    push_pile(&playmap[position[0]+1][position[1]],elt_tile[5]);
    ptile->position[0]=position[0];
    ptile->position[1]=position[1];
    return 0;
  }
  else if (ptile->rotation == 2) {
	  if ((position[0]+3) > size  || (position[1]+2) > size) {
	  fprintf(stderr,"insertion impossible, position out of playmap\n");
	  return 1;
	  }
    push_pile(&playmap[position[0]+2][position[1]+1],elt_tile[0]);
    push_pile(&playmap[position[0]+2][position[1]],elt_tile[1]);
    push_pile(&playmap[position[0]+1][position[1]+1],elt_tile[2]);
    push_pile(&playmap[position[0]+1][position[1]],elt_tile[3]);
    push_pile(&playmap[position[0]][position[1]+1],elt_tile[4]);
    push_pile(&playmap[position[0]][position[1]],elt_tile[5]);
    ptile->position[0]=position[0];
    ptile->position[1]=position[1];
    return 0;
  }
  else if (ptile->rotation == 3) {
	  if ((position[0]+2) > size  || (position[1]+3) > size) {
	  fprintf(stderr,"insertion impossible, position out of playmap\n");
	  return 1;
	  }
    push_pile(&playmap[position[0]+1][position[1]],elt_tile[0]);
    push_pile(&playmap[position[0]][position[1]],elt_tile[1]);
    push_pile(&playmap[position[0]+1][position[1]+1],elt_tile[2]);
    push_pile(&playmap[position[0]][position[1]+1],elt_tile[3]);
    push_pile(&playmap[position[0]+1][position[1]+2],elt_tile[4]);
    push_pile(&playmap[position[0]][position[1]+2],elt_tile[5]);
    ptile->position[0]=position[0];
    ptile->position[1]=position[1];
    return 0;
  }
  else {
    return 1;
  }
}

/* Fonction de retrait d'une tuile */
int remove_tile(pile** grid, int size, int id){
	int i,j;
	for (i=0; i<size; i++){
		for (j=0; j<size; j++){
			if ((grid[i][j]->data).tile==id)
				goto found;
		}
	}
	fprintf(stderr, "La tuile n'a pas pu être trouvée");
	return 0;
	found :
	if ((grid[i+2][j]->data).tile==id){ /* La tuile est dans le sens horizontal*/
		if ((grid[i+1][j]->data).tile!=id || (grid[i+1][j+1]->data).tile!=id || (grid[i][j+1]->data).tile!=id || (grid[i+2][j+1]->data).tile!=id)
			return 0;
		pop_pile(&grid[i][j]);
		pop_pile(&grid[i+1][j]);
		pop_pile(&grid[i+2][j]);
		pop_pile(&grid[i][j+1]);
		pop_pile(&grid[i+1][j+1]);
		pop_pile(&grid[i+2][j+1]);
		return 1;
	}
	if ((grid[i][j+2]->data).tile==id){/* La tuile est dans le sens vertical */
		if ((grid[i][j+1]->data).tile!=id || (grid[i+1][j]->data).tile!=id || (grid[i+1][j+1]->data).tile!=id || (grid[i+1][j+2]->data).tile!=id)
			return 0;
		pop_pile(&grid[i][j]);
		pop_pile(&grid[i+1][j]);
		pop_pile(&grid[i][j+1]);
		pop_pile(&grid[i+1][j+1]); 
		pop_pile(&grid[i][j+2]);
		pop_pile(&grid[i+1][j+2]);
		return 1;
	}
  return 0;
  }



