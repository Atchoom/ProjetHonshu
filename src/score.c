#include "score.h"


int village(pile** grid, int n, int x, int y, int** res, int** visited)
{
  int temp_x,temp_y;
  char temp_field;
  int i;
  /*liste temporaire des cases a visité*/
  int** temp_list = (int**)malloc(n*n*sizeof(int*));
  for(i=0; i<n*n; i++)
    {
      temp_list[i] = (int*)malloc(2*sizeof(int));
    }
  int size = 1;
  temp_list[0][0] = x;
  temp_list[0][1] = y;
  /*initialisation resultat*/
  int res_size = 1;
  res[res_size-1][0] = x;
  res[res_size-1][1] = y;
  while(size != 0)
    {
      size = size -1;
      temp_x = temp_list[size][0];
      temp_y = temp_list[size][1];
      if(temp_x > 0)
	{
	  if((visited[temp_x-1][temp_y] != 1) && empty_pile(grid[temp_x-1][temp_y]) == 0)	
	    {	
	      visited[temp_x-1][temp_y] = 1;	
	      temp_field = (peak_pile(grid[temp_x-1][temp_y])).field;
	      if(temp_field == 'V')
		{
		  temp_list[size][0] = temp_x - 1 ;
		  temp_list[size][1] = temp_y;
		  res[res_size][0] = temp_x - 1 ;
		  res[res_size][1] = temp_y;
		  res_size += 1;
		  size += 1;
		}
	    }
	}
      if(temp_y > 0)
	{
	  if((visited[temp_x][temp_y-1] != 1) && empty_pile(grid[temp_x][temp_y-1]) == 0)
	    {
	      visited[temp_x][temp_y-1] = 1;
	      temp_field = peak_pile(grid[temp_x][temp_y-1]).field;
	      if(temp_field == 'V')
		{
		  temp_list[size][0] = temp_x;
		  temp_list[size][1] = temp_y-1;
		  res[res_size][0] = temp_x;
		  res[res_size][1] = temp_y-1;
		  res_size += 1;
		  size += 1;
		}
	    }
	}
      if(temp_x < n-1)
	{
	  if((visited[temp_x+1][temp_y] != 1) && empty_pile(grid[temp_x+1][temp_y]) == 0)
	    {
	      temp_field = peak_pile(grid[temp_x+1][temp_y]).field;
	      visited[temp_x+1][temp_y] = 1;
	      if(temp_field == 'V')
		{
		  temp_list[size][0] = temp_x + 1 ;
		  temp_list[size][1] = temp_y;
		  res[res_size][0] = temp_x + 1 ;
		  res[res_size][1] = temp_y;
		  res_size += 1;	
		  size += 1;			
		}
	    }
	}
      if(temp_y < n-1)
	{
	  if((visited[temp_x][temp_y+1] != 1) && empty_pile(grid[temp_x][temp_y+1]) == 0)
	    {	
	      temp_field = peak_pile(grid[temp_x][temp_y+1]).field;
	      visited[temp_x][temp_y+1] = 1;				
	      if(temp_field == 'V')
		{
		  temp_list[size][0] = temp_x;
		  temp_list[size][1] = temp_y+1;
		  res[res_size][0] = temp_x;
		  res[res_size][1] = temp_y+1;
		  res_size += 1;	
		  size += 1;		
		}
	    }
	}
    }
  for(i=0;i<n*n;i++)
    {
      free(temp_list[i]);
    }
  free(temp_list);
  return res_size;
}

int lac(pile** grid, int n, int x, int y, int** res, int** visited)
{
  int temp_x,temp_y;
  char temp_field;
  int i;
  /*liste temporaire des cases a visité*/
  int** temp_list = (int**)malloc(n*n*sizeof(int*));
  for(i=0; i<n*n; i++)
    {
      temp_list[i] = (int*)malloc(2*sizeof(int));
    }
  int size = 1;
  temp_list[0][0] = x;
  temp_list[0][1] = y;
  /*initialisation resultat*/
  int res_size = 1;
  res[res_size-1][0] = x;
  res[res_size-1][1] = y;
  while(size != 0)
    {
      size = size -1;
      temp_x = temp_list[size][0];
      temp_y = temp_list[size][1];
      if(temp_x > 0)
	{
	  if((visited[temp_x-1][temp_y] != 1) && empty_pile(grid[temp_x-1][temp_y]) == 0)	
	    {	
	      visited[temp_x-1][temp_y] = 1;	
	      temp_field = (peak_pile(grid[temp_x-1][temp_y])).field;
	      if(temp_field == 'L')
		{
		  temp_list[size][0] = temp_x - 1 ;
					temp_list[size][1] = temp_y;
					res[res_size][0] = temp_x - 1 ;
					res[res_size][1] = temp_y;
					res_size += 1;
					size += 1;
		}
	    }
	}
      if(temp_y > 0)
	{
	  if((visited[temp_x][temp_y-1] != 1) && empty_pile(grid[temp_x][temp_y-1]) == 0)
	    {
	      visited[temp_x][temp_y-1] = 1;
	      temp_field = peak_pile(grid[temp_x][temp_y-1]).field;
	      if(temp_field == 'L')
		{
		  temp_list[size][0] = temp_x;
		  temp_list[size][1] = temp_y-1;
		  res[res_size][0] = temp_x;
		  res[res_size][1] = temp_y-1;
		  res_size += 1;
		  size += 1;
		}
	    }	  
	}
      if(temp_x < n-1)
	{
	  if((visited[temp_x+1][temp_y] != 1) && empty_pile(grid[temp_x+1][temp_y]) == 0)
	    {
	      temp_field = peak_pile(grid[temp_x+1][temp_y]).field;
	      visited[temp_x+1][temp_y] = 1;
	      if(temp_field == 'L')
		{
		  temp_list[size][0] = temp_x + 1 ;
		  temp_list[size][1] = temp_y;
		  res[res_size][0] = temp_x + 1 ;
		  res[res_size][1] = temp_y;
		  res_size += 1;	
		  size += 1;			
		}
	    }
	}
      if(temp_y < n-1)
	{
	  if((visited[temp_x][temp_y+1] != 1) && empty_pile(grid[temp_x][temp_y+1]) == 0)
	    {	
	      temp_field = peak_pile(grid[temp_x][temp_y+1]).field;
	      visited[temp_x][temp_y+1] = 1;				
	      if(temp_field == 'L')
		{
		  temp_list[size][0] = temp_x;
		  temp_list[size][1] = temp_y+1;
		  res[res_size][0] = temp_x;
		  res[res_size][1] = temp_y+1;
		  res_size += 1;	
		  size += 1;		
		}
	    }
	}
    }
  for(i=0;i<n*n;i++)
    {
      free(temp_list[i]);
    }
  free(temp_list);
  return res_size;
}




int max(int a,int b)
{
  if(a>=b)
    return a;
  else
    return b;
}

int min(int a,int b)
{
  if(a>=b)
    return b;
  else
    return a;
}

int score(pile** grid,int n,int*rules,int* detail_score)
{
  int max_village = 0;
  int i,j;
  int squared = 0;
  char field;
  int score;
  int forest_value = 1;
  /*tableau des visités pour les villes*/
  int ** visited = (int**)malloc(n*sizeof(int*));
  for(i=0; i<n; i++)
    {
      visited[i] = (int*)calloc(n,sizeof(int));
    }
  int** curr_village = (int**)malloc(n*n*sizeof(int*));
  for(i=0; i<n*n; i++)
    {
      curr_village[i] = (int*)malloc(2*sizeof(int));
    }
  int** curr_lake = (int**)malloc(n*n*sizeof(int*));
  for(i=0; i<n*n; i++)
    {
      curr_lake[i] = (int*)malloc(2*sizeof(int));
    }
  int** curr_plain = (int**)malloc(n*n*sizeof(int*));
  for(i=0; i<n*n; i++)
    {
      curr_plain[i] = (int*)malloc(2*sizeof(int));
    }
  int* number = (int*)calloc(6,sizeof(int));
  for(i = 0; i < n; i++)
    {
      for(j = 0; j<n; j++)
	{
	  if(empty_pile(grid[i][j])==0)
	    {
	      field = peak_pile(grid[i][j]).field;
	      switch(field)
		{
		case 'P':
		  if (rules[1] == 1)
		    {
		      if (visited[i][j]==0)
			{
			  visited[i][j]=1;
			  if (taille(grid,n,i,j,curr_plain,visited)>=4)
			    {
			      number[0]+=4;
			    }
			}
		    }
		  break;
		case 'F':
		  if (rules[4] == 1)
		    {
		      number[1] += forest_value;
		      if (forest_value < 3)
			{
			  forest_value ++;
			}
		    }
		  else
		    {
		      number[1]+=2;
		    }
		  break;
		case 'L':
		  if (rules[0]==0)
		    {
		      if(visited[i][j] == 0)
			{
			  visited[i][j] = 1;
			  number[2] += 3*(taille(grid,n,i,j,curr_lake,visited)-1);
			}
		    }
		  else
		    {
		      number[2] += 2;
		    }
		  break;
		  case 'V':
		  if(visited[i][j] == 0)
		    {
		      visited[i][j] = 1;
		      max_village = taille(grid,n,i,j,curr_village,visited);
		      if (max_village > number[3])
			{
			  number[3] = max_village;
			  if (rules[3]==1)
			    {
			      squared = square(grid,n,curr_village,number[3]);
			      }
			}
		      if (max_village == number[3])
			{
			  if (rules[3]==1)
			    {
			      squared = squared || square(grid,n,curr_village,number[3]);
			    }
			}
		      
		    }
		    break;
		case 'U':
		  number[4] += 1;
		  break;
		case 'R':
		  number[5] += 1;
		  break;
		}
	    }
	}
    }  
  detail_score[0] = number[1]; /*foret*/
  detail_score[1] = number[2]; /*lac*/
  if (rules[2] == 0){
      detail_score[2] = 4*min(number[4],number[5]);
    }
  else{
    if (number[4] > number[5]/2){detail_score[2] = 4*number[5];}
    else{detail_score[2] = 8*number[4];}
  } /*ressource - usines*/
  detail_score[3] = squared == 0 ? number[3] : number[3] + 4;   /*village*/
  detail_score[4] = number[0]; /*plaine*/
  score = detail_score[0] + detail_score[1] + detail_score[2] + detail_score[3] + detail_score[4]; 
  for(i=0;i<n;i++)
    {
      free(visited[i]);
    }
  free(visited);
  for(i=0;i<n*n;i++)
    {
      free(curr_lake[i]);
      free(curr_village[i]);
      free(curr_plain[i]);
    }
  free(curr_plain);
  free(curr_lake);
  free(curr_village);
  free(number);
  return score;
}

int pot(pile p,int*rules)
{
  int res;
  if(empty_pile(p))
    {
      return 0;
    }  
  switch(peak_pile(p).field)
    {
    case 'P':
      res = rules[1] == 0 ? 0 : 1;
      break;
    case 'F' :
      res = rules[4] == 0 ? 2 : 3;
      break;
    case 'V' :
      res = rules[3]==0 ? 1 : 2;
      break;
    case 'U' :
      res = rules[2]==0 ? 4 : 8;
      break;
    case 'R' :
      res = 4;
      break;
    case 'L':
      res = rules[0]==0 ? 3 : 2;
    default:
      res = 0;
      break;
    }
  return res;
}

int potentiel(tile t,int*rules)
{
  int i;
  int res=0;
  for(i = 0; i<6; i++)
    {
      switch(t.fields[i])
	{
	case 'P' :
	  res += rules[1] == 0 ? 0 : 1;
	case 'F' :
	  res += rules[4]==1 ? 3 : 2;
	  break;
	case 'V' :
	  res += rules[3]==0 ? 1 : 2;
	  break;
	case 'L' :
	  res += rules[0]==0 ? 3 : 2;
	  break;
	case 'U':
	  res += rules[2]==0 ? 4 : 8;
	  break;
	case 'R':
	  res += 4;
	  break;
	}
    }
  return res;
}

int potentiel_lost(game g, tile t)
{
  int x = t.position[0];
  int y = t.position[1];
  int rota = t.rotation;
  int res = 0;
  if (rota == 0) {
    res += pot(g.grid[x][y],g.rules);
    res += pot(g.grid[x][y+1],g.rules);
    res += pot(g.grid[x+1][y],g.rules);
    res += pot(g.grid[x+1][y+1],g.rules);
    res += pot(g.grid[x+2][y],g.rules);
    res += pot(g.grid[x+2][y+1],g.rules);
  }
  else if (rota == 1) {
    res += pot(g.grid[x][y+2],g.rules);
    res += pot(g.grid[x+1][y+2],g.rules);
    res += pot(g.grid[x][y+1],g.rules);
    res += pot(g.grid[x+1][y+1],g.rules);
    res += pot(g.grid[x][y],g.rules);
    res += pot(g.grid[x+1][y],g.rules);
  }
  else if (rota == 2) {
    res += pot(g.grid[x+2][y+1],g.rules);
    res += pot(g.grid[x+2][y],g.rules);
    res += pot(g.grid[x+1][y+1],g.rules);
    res += pot(g.grid[x+1][y],g.rules);
    res += pot(g.grid[x][y+1],g.rules);
    res += pot(g.grid[x][y],g.rules);
  }
  else if (rota == 3) {
    res += pot(g.grid[x+1][y],g.rules);
    res += pot(g.grid[x][y],g.rules);
    res += pot(g.grid[x+1][y+1],g.rules);
    res += pot(g.grid[x][y+1],g.rules);
    res += pot(g.grid[x+1][y+2],g.rules);
    res += pot(g.grid[x][y+2],g.rules);
  }
  return res;
}

int taille(pile** grid, int n, int x, int y, int** res, int** visited)
{
  int temp_x,temp_y;
  char temp_field;
  char request_field = peak_pile(grid[x][y]).field;
  int i;
  /*liste temporaire des cases a visité*/
  int** temp_list = (int**)malloc(n*n*sizeof(int*));
  for(i=0; i<n*n; i++)
    {
      temp_list[i] = (int*)malloc(2*sizeof(int));
    }
  int size = 1;
  temp_list[0][0] = x;
  temp_list[0][1] = y;
  /*initialisation resultat*/
  int res_size = 1;
  res[res_size-1][0] = x;
  res[res_size-1][1] = y;
  while(size != 0)
    {
      size = size -1;
      temp_x = temp_list[size][0];
      temp_y = temp_list[size][1];
      if(temp_x > 0)
	{
	  if((visited[temp_x-1][temp_y] != 1) && empty_pile(grid[temp_x-1][temp_y]) == 0)	
	    {	
	      visited[temp_x-1][temp_y] = 1;	
	      temp_field = (peak_pile(grid[temp_x-1][temp_y])).field;
	      if(temp_field == request_field)
		{
		  temp_list[size][0] = temp_x - 1 ;
		  temp_list[size][1] = temp_y;
		  res[res_size][0] = temp_x - 1 ;
		  res[res_size][1] = temp_y;
		  res_size += 1;
		  size += 1;
		}
	    }
	}
      if(temp_y > 0)
	{
	  if((visited[temp_x][temp_y-1] != 1) && empty_pile(grid[temp_x][temp_y-1]) == 0)
	    {
	      visited[temp_x][temp_y-1] = 1;
	      temp_field = peak_pile(grid[temp_x][temp_y-1]).field;
	      if(temp_field == request_field)
		{
		  temp_list[size][0] = temp_x;
		  temp_list[size][1] = temp_y-1;
		  res[res_size][0] = temp_x;
		  res[res_size][1] = temp_y-1;
		  res_size += 1;
		  size += 1;
		}
	    }
	}
      if(temp_x < n-1)
	{
	  if((visited[temp_x+1][temp_y] != 1) && empty_pile(grid[temp_x+1][temp_y]) == 0)
	    {
	      temp_field = peak_pile(grid[temp_x+1][temp_y]).field;
	      visited[temp_x+1][temp_y] = 1;
	      if(temp_field == request_field)
		{
		  temp_list[size][0] = temp_x + 1 ;
		  temp_list[size][1] = temp_y;
		  res[res_size][0] = temp_x + 1 ;
		  res[res_size][1] = temp_y;
		  res_size += 1;	
		  size += 1;			
		}
	    }
	}
      if(temp_y < n-1)
	{
	  if((visited[temp_x][temp_y+1] != 1) && empty_pile(grid[temp_x][temp_y+1]) == 0)
	    {	
	      temp_field = peak_pile(grid[temp_x][temp_y+1]).field;
	      visited[temp_x][temp_y+1] = 1;				
	      if(temp_field == request_field)
		{
		  temp_list[size][0] = temp_x;
		  temp_list[size][1] = temp_y+1;
		  res[res_size][0] = temp_x;
		  res[res_size][1] = temp_y+1;
		  res_size += 1;	
		  size += 1;		
		}
	    }
	}
    }
  for(i=0;i<n*n;i++)
    {
      free(temp_list[i]);
    }
  free(temp_list);
  return res_size;
}

int square(pile**grid,int n,int **zone, int size)
{
  int k;
  int i,j;
  int t0,t1,t2,t3,t4,t5,t6,t7;
  char request_field = 'V';
  for (k=0;k<size;k++)
    {
      i = zone[k][0];
      j = zone[k][1];
      t0 = i>0 && j>0 && !empty_pile(grid[i-1][j-1]) && peak_pile(grid[i-1][j-1]).field == request_field;
      t1 = i>0 && !empty_pile(grid[i-1][j]) && peak_pile(grid[i-1][j]).field == request_field;
      t2 = i>0 && j<n-1 && !empty_pile(grid[i-1][j+1]) && peak_pile(grid[i-1][j+1]).field == request_field;
      t3 = j>0 && !empty_pile(grid[i][j-1]) && peak_pile(grid[i][j-1]).field == request_field;
      t4 = j<n-1 && !empty_pile(grid[i][j+1]) && peak_pile(grid[i][j+1]).field == request_field;
      t5 = i<n-1 && j>0 && !empty_pile(grid[i+1][j-1]) && peak_pile(grid[i+1][j-1]).field == request_field;
      t6 = i<n-1 && !empty_pile(grid[i+1][j]) && peak_pile(grid[i+1][j]).field == request_field;
      t7 = i<n-1 && j<n-1 && !empty_pile(grid[i+1][j+1]) && peak_pile(grid[i+1][j+1]).field == request_field;
      if ((t0 && t1 && t3) || (t1 && t2 && t4) || (t3 && t5 && t6) || (t4 && t6 && t7))
	{
	  return (1==1);
	}
    }
  return (1==0);
}

