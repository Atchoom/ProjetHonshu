#include "list.h"
#include <stdio.h>
#include <stdlib.h>


list create_list()
{
	return NULL;
}

void push_list(list *l, tile t)
{
	list new = malloc(sizeof(struct node));
	new->data = t;
	new->next = *l;
	*l = new;
}

void add_list(list *l, tile t, int index)
{
	if(index == 0)
	{
		push_list(l,t);
	}
	else
	{
		int i=0;
		list next = *l;
		list current = *l;
		list new = malloc(sizeof(struct node));
		while(!(next==NULL) && (i < index))
		{
			current = next;
			next = current->next;
			i++;
		}
		new -> data = t;
		new -> next = next;
		current -> next = new;
	}

}

tile remove_list(list *l, int i)
{
	if (l == NULL)
	{
		printf("Liste vide\n");
		exit(0);
	}
	tile res;
	list current = *l;
	list last = NULL;
	int j = 1;
	while( (current != NULL)&&j<i)
	{
		j++;
		last = current;
		current = current -> next;
	}
	if(j<i)
		{
			printf("index trop grand comparé à la taille de la liste\n");
			exit(0);
		}
	res = current->data;
	if(last == NULL)
	{
		last = *l;
		*l = (*l) -> next;
		free(last);
	}
	else
	{
		last->next = current -> next;
		free(current);		
	}	
	return res;
}

tile display_list(list l, int i)
{
	if (l == NULL)
	{
		printf("Liste vide\n");
		exit(0);
	}
	list current = l;
	tile res;
	int j = 1;
	while((current != NULL) &&j<i)
	{
		j++;
		current = current -> next;
	}
	if(j<i)
		{
			printf("index %d trop grand comparé à la taille de la liste\n",i);
			exit(0);
		}
	res = current -> data;
	return res;

}

void print_list(list l)
{
	list current = l;
	while(current != NULL)
	{
		print_tile(current->data);
		printf("\n");
		current = current->next;
	}
}

void free_list(list l)
{
	list current = l;
	list next = l;
	while(current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}
}

int length_list(list l)
{
  int count = 0;
  while (l != NULL)
    {
      l = l->next;
      count ++;
    }
  return count;
}

list copy_list(list l, int n)
{
  list res = create_list();
  list current = res;
  list new;
  int count = 0;
  while (l != NULL && count < n)
    {
      new = malloc(sizeof(struct node));
      new -> data = l -> data;
      new -> next = NULL;
      if (res == NULL)
	{
	  res = new;
	}
      else {
	current -> next = new;
      }
      current = new;
      l = l -> next;
      count++;
    }
  return res;
}
