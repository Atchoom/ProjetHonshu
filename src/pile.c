#include "pile.h"


pile create_pile()
{
	return NULL;
}


void push_pile(pile *p, element elt)
{
  	struct square *new = malloc(sizeof(struct square));
	new->data = elt;
	new->next = *p;
	*p = new;
}

element pop_pile(pile *p)
{
	element res = (*p)->data;
	pile temp = (*p)->next;
	free(*p);
	(*p) = temp;
	return res;
}

void print_pile(pile p)
{
	pile current = p;
	while (current != NULL)
	{
		printf("%c ",(current->data).field);
		current = current -> next;
	}
	printf("\n");
}



void print_top_pile(pile p) {
	pile current = p;
	if (current == NULL) {
		printf("*");
	}
	else {
		printf("%c", (current->data).field);
	}
}

void free_pile(pile p)
{
	pile current = p;
	pile next = p;
	while(current != NULL)
	{
		next = current->next;
		free(current);
		current = next;
	}
}

element peak_pile(pile p)
{	
  return p->data;
}

element peak_last_pile(pile p)
{
  if (p == NULL)
    {
     fprintf(stderr,"pile vide");
      exit(1);
    }
  while (p->next != NULL)
    {
      p=p->next;
    }
  return(p->data);
}

int empty_pile(pile p)
{
  return p == NULL;
}
