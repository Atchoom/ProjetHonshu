#ifndef LIST_H
#define LIST_H
#include "grid.h"

/**
 * \file list.h
 * \brief Définit les listes de tuiles
 * \author ByeZoé
 * \date jeudi 22 mars
 *
 * Ce fichier définit les listes de tuiles et des fonctions basiques pour manipuler les éléments de la liste. Il contient 2 types et 9 fonctions:
 * - le type \a list définissant une liste chaînées de tuiles
 * - le type \a square définissant un maillon d'une liste
 * - \b create_list() pour créer une liste vide
 * - \b push_list(&l,t) pour ajouter une tuile en tete de la liste
 * - \b add_list(l,t,index) pour rajouter une tuile à index dans la liste
 * - \b remove_list(&l,i) pour retirer une tuile d'une liste
 * - \b display_list(l,i) pour renvoyer une tuile de la liste
 * - \b print_list(l) pour afficher les tuiles d'une liste
 * - \b free_list(l) pour détruire une liste et libérer la mémoire
 * - \b length_list(l) pour renvoyer la taille d'une liste
 * - \b copy_list(l,n) pour copier les n premiers éléments d'une liste
 */

/**
 * \struct node
 * \brief Composant d'une liste
 * \details Maillon d'une liste contenant une data (tile), et un pointeur vers la suite de la liste
 */
struct node{
  tile data;
    /**< Une tuile */
  struct node*next;
/**< Pointeur vers la suite de la liste */
};

/**
* \typedef list
* \brief Type définissant les listes chaînées.
* \details Une liste chaînée est une liste de tuile.
*/
typedef struct node* list;

/**
 * \brief Crée une liste vide.
 * \details Renvoie une liste vide
 * \return Une liste vide
 */
list create_list();

/**
 * \brief Ajoute une tuile dans une liste.
 * \details Alloue la mémoire pour un noeud et ajoute ce noeud en tête de liste
 * \param l Pointeur vers une liste
 * \param t Tuile à ajouter à la liste
 */
void push_list(list *l, tile t);

/**
 * \brief Ajoute une tuile dans une liste a la position index.
 * \details Alloue la mémoire pour un noeud et ajoute ce noeud à la position index dans la liste.
 * \param l Pointeur vers une liste
 * \param t Tuile à ajouter à la liste
 * \param index Indice pour savoir ou ajouter la tuile, c'est un entier naturel positif.
 * \attention Si l'indice est supérieur à la taille de la liste, la tuile est ajouté automatiquement à la fin.
 */
void add_list(list *l, tile t, int index);

/**
 * \brief Retire une tuile de la liste.
 * \details Retire le noeud dont la position dans la liste est passée en paramètre. Libère la mémoire du noeud supprimé. Renvoie une erreur si la liste n'est pas assez longue.
 * \param l Une liste de taille au moins \a i
 * \param i Le numéro de la tuile à retirer
 * \return La tuile retirée
 * \attention La liste doit être de taille au moins \a i
 */
tile remove_list(list *l, int i);

/**
 * \brief Renvoie une tuile de la liste.
 * \details Fonction qui prend un entier i en paramètre et renvoie la i-ème tuile d'une liste. Renvoie une erreur si la liste est trop petite. 
 * \param l Une liste de taille au moins \a i
 * \param i Le numéro de la tuile à renvoyer
 * \return La i-ème tuile de la liste
 * \attention La liste doit être de taille au moins \a i
 */
tile display_list(list l, int i);

/**
 * \brief Affiche une liste.
 * \details Affiche les tuiles les unes après les autres.
 * \param l Une liste à afficher
 */
void print_list(list l);

/**
 * \brief Détruit une liste.
 * \details Libère la mémoire de tous les noeuds de la liste.
 * \param l Une liste à détruire
 */
void free_list(list l);

/**
 * \brief Renvoie la taille de la liste.
 * \details Calcule et renvoie la longueur de la liste passée en paramètre.
 * \param l Une liste
 * \return La taille de la liste
 */
int length_list(list l);

/**
 * \brief Copie les n premiers éléments d'une liste.
 * \details Alloue la mémoire pour une nouvelle liste et crée une nouvelle liste indépendante avec les mêmes n premières valeurs que la liste passée en paramètre. Si la liste est plus petite que n, alors la liste entière est copiée.
 * \param l Une liste
 * \param n Un entier positif.
 * \return Une copie de l
 */
list copy_list(list l, int n);
#endif
