#ifndef INIT_H
#define INIT_H
#include <time.h>
#include "pile.h"
#include "grid.h"
#include "list.h"
#define MAX_SIZE 25

/**
 * \file init.h
 * \brief Gère l'initialisation d'une grille
 * \author ByeZoé
 * \date jeudi 22 mars
 *
 * Ce fichier définit la structure \a game et des fonctions pour la manipuler ou l'initialiser, en outre il permet d'importer une une grille ou d'en exporter une. Il contient un type et 8 fonctions:
 * - le type \a game contenant la grille et les tuiles existantes
 * - \b rand_int(inf,sup) pour renvoyer un entier entre inf et sup
 * - \b random_rule(nb_rule,n) pour déterminer les règles aléatoires à appliquer
 * - \b next_int(f,addr,next) pour lire le prochain int dans un fichier
 * - \b in_tab(t,i,size) pour si tester un entier est dans un tableau
 * - \b init_game(n) pour initialiser une partie avec des données aléatoires
 * - \b init_game_f(g,addr_tile,addr_game) pour importer une partie à partir de fichiers
 * - \b create_files(size,nb_tiles,hand,hd_lg,id_start,tiles) pour généerer des fichiers de partie.
 * - \b print_game(g) pour afficher une partie
 */

/** 
 *\struct game
 *\brief Structure de données pour la partie
 */
typedef struct game {
	int lg;
  /**< Un entier représentant la taille de la grille */
	int hd_lg;
  /**< Un entier précisant la taille de la main du joueur */
	pile** grid;
  /**< Une grille de jeu */
	list hand;
  /**< Une liste de tuiles, celle dans la main du joueur */
	list posed;
  /**< Une liste de tuiles posées */
  int *rules;
  /**< Un entier désigneant en base 6 les règles utilisées */
} game;

/** 
 * \brief Renvoie un entier aléatoire entre deux bornes.
 * \details Renvoie un entier aléatoirement sélectionné dans [inf,sup[
 * \param inf La valeur minimale
 * \param sup La valeur maximale
 * \return Un entier compris entre inf et sup
 * \attention inf doit être plus petit que sup
 */
int rand_int(int inf, int sup);

/**
 * \brief Génère les règles aléatoirement
 * \details Génère un tableau de taille nb_rule représentant les règles contenant des 0 ou des 1. La première valeur (indice 0) correspond à la première règle, la deuxième (indice 1) à la deuxième et ainsi de suite. La valeur 0 signifie que la règle associé n'est pas active, la valeur 1 signifie que la règle est active. Le paramètre n détermine le nombre de règles à utiliser, si n est négatif alors le nombre de règles est également aléatoire. n doit impérativement être plus petit que nb_rule.
 * \param nb_rule Un entier strictement positif, le nombre de règles parmi lesquelles on choisit.
 * \param n Un entier inférieur ou égal à nb_rule, le nombre de règle choisi.
 * \return Un tableau de taille nb_rule contenant des 0 et des 1, si n est positif alors e tableau contient n valeur 1.
 * \attention n doit être inférieur ou égal à nb_rule.
 */
int*random_rule(int nb_rule, int n);

/** 
 * \brief Renvoie le prochain entier lu dans un fichier.
 * \details Dans un fichier, lit le prochain groupe de caractère, c'est-à-dire jusqu'au prochain espace ou saut de ligne
 * \param f Un fichier
 * \param addr le chemin vers ce fichier
 * \param next pointeur vers un entier pour stocker l'entier lu
 * \return 0 si il lit sans problème, 1 sinon
 * \attention Le fichier pointé par f doit être accessible en lecture et doit être bien défini.
 * \attention Les caractères lu doivent être des entiers
 */
int next_int(FILE * f, char* addr, int* next);

/** 
 * \brief Teste si un entier est dans un tableau.
 * \details Parcourt le tableau d'entier passé en paramètre et renvoie 1 (True) si l'entier est trouvé et 0 (False) sinon.
 * \param t Un tableau d'entier de taille size
 * \param i Un entier à rechercher dans le tableau
 * \param size La taille du tableau \a t
 * \return La réponse au prédicat 'i appartient à t'
 * \attention t doit être de taille size et un non NULL
 */
int in_tab(int* t,int i, int size);

/** 
 * \brief Génère une \a game aléatoire.
 * \details Génère aléatoirement une main, une grille, une première tuile placée au centre.
 * \param n Le nombre de règles modifiées désiré
 * \return Une \a game aléatoirement initialisée 
 */
game init_game (int n);

/** 
 * \brief Génère une \a game aléatoire, mais on peut choisir la taille.
 * \details Génère aléatoirement une main, une grille de taille size, une première tuile placée au centre.
 * \param n Le nombre de règles modifiées désiré
 * \param size Un entier naturel supérieur à 8 pour la taille de la grille
 * \return Une \a game aléatoirement initialisée 
 */
game init_game2 (int n, int size);

/** 
 * \brief Initialise une grille à partir de fichiers.
 * \details Importe les informations contenues dans les fichiers pointés par les paramètres et stocke la partie correspondante. Renvoie 0 si tout est réussis, 1 sinon. Affiche les messages d'erreurs en cas de problème d'ouverture ou de lecture.
 * \param g Pointeur vers une structure game
 * \param addr_tile Addresse vers un fichier contenant les informations du jeu de tuiles
 * \param addr_game Addresse vers un fichier contenant les informations d'une partie
 * \return 0 si réussis, 1 sinon 
 */
int init_game_f(game *g, char* addr_tile, char* addr_game);

/** 
 * \brief Exporte les données de la partie dans deux fichiers.
 * \details Génère les deux fichiers d'initialisation de jeu à partir des paramètres
 * \param size Taille de la grille, un entier positif
 * \param nb_tiles Nombre de tuiles dans la liste, un entier positif
 * \param hand Tableau d'entiers contenant les numéros des tuiles de la main
 * \param hd_lg Taille de la main, un entier positif
 * \param id_start Numéro de la tuile de départ dans la liste, un entier naturel inférieur au nombre de tuiles
 * \param tiles Liste de tuiles, du type list, de taille nb_tiles
 * \return 0 si la fonction s'est entièrement déroulée,0 sinon.
 */
int create_files(int size,int nb_tiles, int* hand, int hd_lg, int id_start, list tiles);

/** 
 * \brief Enleve la derniere tuile posée
 * \details Retire la dernière tuile posée de la partie, c'est à dire la dernière tuile de la liste posed.
 * \param g la partie
 * \return Un entier, 1 si la tuile a été retirée, 0 sinon.
 */
int remove_last_tile(game* g);
#endif
