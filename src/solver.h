#ifndef SOLVER_H
#define SOLVER_H
#include "init.h"
#include "affichage.h"
#include "recouvrement.h"
#include <math.h>
#include "score.h"
#include <time.h>
#include "pile.h"
#include <string.h>	
#include "grid.h"
#include "list.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * \file solver.h
 * \brief Construction des fonctions nécessaires aux solveurs et des solveurs
 * \author ByeZoé
 * \date Avril
 *
 * Ce fichier définit les fonctions des solveurs. Il contient 6 fonctions:
 * - \b best_tile(g,id_res,score_res,x_res,y_res,rota_res,detail_score) Recherche la tuile et la position renvoyant le meilleur score.
 * - \b best_position(g,current_tile,score_res,x_res,y_res,rota_res,detail_score) Recherche la position d'une tuile donnée renvoyant le meilleur score.
 * - \b insert_potentiel(game,res,t) insert une tuile dans une liste en la gardant triée en fonction du potentiel
 * - \b tri_insert_potentiel(game,res,l) renvoie la liste triée en fonction du potentiel 
 * - \b solver_glo(g) solveur via un algorithme glouton
 * - \b solver_glo_plot(g) solveur glouton par potentiel de tuile
 */

/**
 * \brief Recherche la meilleure tuile et sa position  a un tour de jeu
 * \details Pour chaque tuile de la main, et chaque position et rotation, on calcule le score obtenu et le potentiel perdu, et on en déduit la meilleure tuile. 
 * \param g un pointeur vers une partie
 * \param id_res un pointeur vers un entier pour stocker l'identifiant de la tuile
 * \param score_res un pointeur vers un entier pour stocker le score
 * \param x_res un pointeur vers un entier pour stocker une coordonnée de la tuile
 * \param y_res un pointeur vers un entier pour stocker une coordonnée de la tuile
 * \param rota_res un pointeur vers un entier pour stocker la rotation de la tuile
 * \param detail_score un tableau d'entiers pour stocker le détail du score.  
 */
void best_tile(game* g, int* id_res, int* score_res, int* x_res, int* y_res, int* rota_res, int* detail_score);


/**
 * \brief Recherche la meilleure position pour une tuile  a un tour de jeu
 * \details Pour chaque position et rotation de la tuile , on calcule le score obtenu et le potentiel perdu, et on en déduit la meilleure tuile. 
 * \param g un pointeur vers une partie
 * \param current_tile la tuile a étudié
 * \param score_res un pointeur vers un entier pour stocker le score
 * \param x_res un pointeur vers un entier pour stocker une coordonnée de la tuile
 * \param y_res un pointeur vers un entier pour stocker une coordonnée de la tuile
 * \param rota_res un pointeur vers un entier pour stocker la rotation de la tuile
 * \param detail_score un tableau d'entiers pour stocker le détail du score.  
 */
void best_position(game* g,tile current_tile, int* score_res, int* x_res, int* y_res, int* rota_res, int* detail_score);

/**
 * \brief Insert dans une liste triée une tuile
 * \details Insert une tuile dans une liste triée par potentiel et la garde triée
 * \param g La partie jouée
 * \param res un pointeur vers une liste triée 
 * \param t une tuile à inserer
 */
void insert_potentiel (game g,list* res, tile t);

/**
 * \brief Tri une liste en fonction du potentiel
 * \details Tri une liste en fonction du potentiel et stock la liste triée
 * \param g La partie jouée
 * \param res un pointeur vers une liste pour stocker la liste triée
 * \param l la liste à trier
 */
void tri_insert_potentiel(game g,list *res, list l);

/**
 * \brief Un solveur par méthode glouton
 * \details Joue une partie en choississant à chaque tour la meilleure tuile, c'est-à-dire celle rapportant le plus de point et ne perdant pas trop de potentiel à la partie.
 * \param g un pointeur vers une partie
 */
void solver_glo(game* g);

/**
 * \brief Un solveur par une méthode glouton différente
 * \details Trie d'abord la main par potentiel. Joue ensuite chaque tuile par ordre de potentiel (en commençant par les plus petits), et la place à la meilleure position.
 * \param g un pointeur vers une partie
 */
void solver_glo_pot(game* g);




#endif

