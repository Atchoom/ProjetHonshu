#include "init.h"
#include "affichage.h"
#include "recouvrement.h"
#include "solver.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{	
	system("clear");
	/*buffers*/
	int i;
	char temp;
	int useless;
	int fin =0;/*Savoir si la game est finie ou pas*/
	int choice;
	int select;/*Id de la tuile*/
	tile selected_tile;/*Tuile choisie*/
	int rota;/*Rotation choise*/
	int position[2];/*Position choisie*/
	int rules=0;/*pour tester que les regles sont bien respectées à chaque insert*/
	game g;
	int* detail = (int*)malloc(4*sizeof(int));
	/*Menu de départ*/
	choice = start_menu(&g);
	system("clear");
	while (choice == 0)
	  {
	    /*En cas d'erreur, on peut revenir au début de la création*/
	    printf("Voulez-vous retentez [y ou n] ?:");
	    scanf("%s",&temp);
	    if (temp == 'y')
		{
		  system("clear");
		  choice = start_menu(&g);			
		}
	    else
		{
		  return 0;
		}
	  }
	/*Boucle de jeu*/
	while(fin == 0)
	{
	  system("clear");
	  print_game(g);
	  score(g.grid,g.lg,g.rules,detail);
	  print_choice(&choice);
	  switch (choice)
	    {
	    case 1:
	      /*On tente l'insertion*/
	      rules = 0;
	      while(rules==0)
		{
		  select_tile(g,&selected_tile,&select);
		  printf("\nOu voulez-vous la placer ?\n");
		  printf("Coordonnée X:");
		  scanf("%d",&position[0]);
		  printf("Coordonnée Y:");
		  scanf("%d",&position[1]);
		  rotate_tile(&rota,&selected_tile);
		  /*On verifie que les regles de pose sont respectées*/
		  if (can_insert(rota,g.lg,position) == 1)
		    {
		      rules = covering_tile_test(g,selected_tile,position)*
			covered_lake(g.grid,position,rota);
		      for(i=0;i<length_list(g.posed);i++)
			{
			  rules = rules * tile_covered_test(g,display_list(g.posed,i),position,rota);
			}
		      if (rules == 0)
			{
			  for(i=0;i<14;i++)
			    {
			      fputs("\033[A\033[2K",stdout);
			    }
			  rewind(stdout);
			  printf("Erreur de position\n");
			  printf("Revenir au menu de sélection d'action ou réessayer de placer une tuile ?[y or n] :");
			  scanf("%s",&temp);
			  if(temp != 'n')
			    {
			      fputs("\033[A\033[2K",stdout);
			      break;
			    }
			}
		    }
		  else
		    {
		      for(i=0;i<14;i++)
			{
			  fputs("\033[A\033[2K",stdout);
			}
		      rewind(stdout);
		      printf("Erreur de position\n");
		    }
		}
	      if(rules == 0)
		{
		  break;
		}
	      /*On insert*/
	      if (insert_tile(g.grid,g.lg,&selected_tile,position) == 1)
		{
		  printf("Revenir au menu:");
		  scanf("%s",&temp);
		  break;	
		}
	      g.hd_lg -= 1;
	      push_list(&(g.posed),selected_tile);
	      remove_list(&(g.hand),select);
	      break;
	    case 2:
	      best_tile(&g,&select,&useless,&position[0],&position[1],&rota,detail);
	      printf("Conseil :\n Tuile %d en %d %d avec une rotation %d\n",select,position[0],position[1],rota);
	      printf("Voulez-vous la jouer ?[y or n]\n");
	      scanf("%s",&temp);
	      if( temp == 'y')
		{
		  g.hd_lg -= 1;
		  selected_tile = remove_list(&(g.hand),select);
		  push_list(&(g.posed),selected_tile);
		  insert_tile(g.grid,g.lg,&selected_tile,position);
		}
	      break;
	    case 3:
	      solver_glo(&g);
	      break;
	    case 4:
	      print_rule(g);
	      printf("\n\nEcrivez quelquechose pour revenir au jeu\n");
	      scanf("%s",&temp);
	      break;
	    case 5:
	      fin = 1;
	      break;
	    default:
	      printf("Valeur non valide :(\n");
	      break;	
	    }
	  /*On test si la partie est finie*/
	  if(fin != 1)
	    {
	      fin = game_over(g);
	    }	
	}
	system("clear");
	printf("Game finie, c'était génial hein ?\n");
	print_game(g);
	free_grid(g.grid,g.lg);
	free_list(g.hand);
	free_list(g.posed);
	free(detail);	
	return 0;
}
