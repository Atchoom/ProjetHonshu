#include "affichage.h"
#include <unistd.h>

/*Fonction print d'une partie */
void print_game(game g)
{
  	int i,j;
  	int* detail_score = (int*)malloc(5*sizeof(int));
  	int score_res = 0;
  	list hand = g.hand;
  	char *temp = "";
  	printf("Main :\n");
  	temp = "    ";
  	for (i = 1; i < g.hd_lg+1; i++)
  	{
  		if(i == (g.hd_lg))
  		{
  			temp = "\n";
  		}
  		printf("%2d%s",i,temp);
  	}
  	for(j=0;j<6;j = j + 2)
  	{
  		temp = "   ";
	  	for (i = 1; i < g.hd_lg+1; i++)
	  	{
	  		if(i == g.hd_lg)
	  		{
	  			temp = "\n";
	  		}
	  		printf("%c %c%s",(display_list(hand,i)).fields[j],(display_list(hand,i)).fields[j+1],temp);
	  	}
 	}
 	printf("\n");
 	printf("Grille de jeu:\n");
 	printf("\n    ");
 	for (i=0;i<g.lg;i++)
 	{
 			printf("%3d",i);
 	}
 	printf("\n\n");
 	for(i=0;i<g.lg;i++)
 	{
 		printf(" %2d ",i);
 	  for(j=0;j<g.lg;j++)
 	  {
 	  	printf("  ");
 	  	print_top_pile(g.grid[i][j]);
 	  }
 	  printf("\n\n");
 	}
 	score_res = score(g.grid,g.lg,g.rules,detail_score);
	printf("SCORE  : %d\n",score_res);
	printf(" F  L  R  V  P\n");
	for(i = 0; i <5; i++)
	{
		printf("%2d ",detail_score[i]);
	}		
	printf("\n");
	free(detail_score);  

}

void select_tile(game g, tile* selected_tile, int* select)
{
	int valid = 0;
	while(valid == 0)
	{
	printf("Choisissez une tuile à jouer:");
	scanf("%2d",select);
	if(*select > g.hd_lg || *select < 1)
		{
		printf("Valeur invalide... :(\n");	
		}
	else
		{
		valid = 1;
		}
	}
	*selected_tile = display_list(g.hand,*select);
	print_tile(*selected_tile);
}

void rotate_tile(int* selected_rotation, tile* selected_tile)
{
	int valid = 0;
	char answer = 'n';
	while(valid == 0)
	{
		printf("Tuile selectionnée :\n");
		print_tile(*selected_tile);
		printf("Voulez-vous tourner la tuile [y or n] :");
		scanf(" %c",&answer);
		if (answer == 'y')
		{
			rotate(selected_tile);
    		fputs("\033[A\033[2K",stdout);
    		fputs("\033[A\033[2K",stdout);
    		fputs("\033[A\033[2K",stdout);
    		fputs("\033[A\033[2K",stdout);
    		fputs("\033[A\033[2K",stdout);
    		fputs("\033[A\033[2K",stdout);
    		rewind(stdout);
		}
		else if (answer == 'n')
		{
			valid = 1;
		}
		else
		{
			printf("Veuillez répondre correctement STP \n");
		}
	}
	*selected_rotation = selected_tile->rotation;
}

void print_choice(int* choice)
{
	int i;
	printf("\nQue voulez-vous faire ?\n\n");
	printf(" - 1 Selectionner une tuile\n\n");
	printf(" - 2 Je sais pas quoi jouer\n\n");
	printf(" - 3 Finir la game à ma place \n\n");
	printf(" - 4 Comment gagner ? \n\n");
	printf(" - 5 Arrêter de jouer car c'est nul\n\n");
	printf("Entrez une valeur :");
	scanf("%d",choice);
	for(i=0;i<11;i++)
	{
		fputs("\033[A\033[2K",stdout);
	}
    rewind(stdout);
}

int start_menu(game* g)
{
	int res = 0;
	char *addr_game= (char*)malloc(50*sizeof(char));
	char *addr_tile =(char*)malloc(50*sizeof(char)); 
	int start;
	int menu1 = 1;
	int menu2;
	int nb; /* nb de règles à changer */
	while (menu1)
	  {
	    printf("   Bienvenue dans Honshu\n\nC'est un super jeu de réflexion à jouer seul, parce que tu es seul.. :( \n\n Made in ByZoe (c'est nous)\n");
	    printf("\nQue voulez-vous faire ?\n\n");
	    printf(" - 1 Jouer une partie aléatoire\n\n");
	    printf(" - 2 Jouer une partie à partir de fichiers\n\n");
	    printf(" - 3 Rien\n\n");
	    scanf("%d",&start);
	    switch(start)
	      {
	      case 1:
	        menu2 = 1;
		system("clear");
		while (menu2)
		  {
		    printf("\nQuelles règles voulez vous ?\n\n");
		    printf(" - 1 Jeu vanilla\n\n");
		    printf(" - 2 Règles arrangées\n\n");
		    printf(" - 3 J'ai changé d'avis\n\n");
		    scanf("%d",&start);
		    switch(start)
		      {
		      case 1:
		      printf("Entrez la taille de la grille : ");
		      int size;
		      scanf("%i", &size);
			*g = init_game2(0, size);
			printf("Game créée :)\n");
			res = 1;
			menu1 = 0;
			menu2 = 0;
			break;
		      case 2:
			printf("Veuillez sélectionner le nombre de règle à modifier entre 0 et 5\n\n");
			scanf("%d",&nb);
			if (nb > 5 || nb < 0)
			  {
			    system("clear");
			    printf("Nombre incorrect\n");
			    break;
			  }
			else
			  {
			    printf("Nombre correct\n");
			    *g = init_game(nb);
			    printf("Game créée :)\n");
			    res = 1;
			    menu2 = 0;
			    menu1 = 0;
			  }
		      case 3:
			menu2=0;
			system("clear");
			break;
		      default:
			printf("Veuillez rentrer une réponse valide\n\n");
		      }
		  }
		break;
	      case 2:
		printf("Rentrez les adresses des fichiers à lire:\n");
		printf("Fichier de jeu:");
		scanf("%s",addr_game);
		printf("Fichier des tuiles:");
		    scanf("%s",addr_tile);
		    if( init_game_f(g,addr_tile,addr_game) == 1)
		      {
			printf("Erreur de création via fichiers...Unlucky :(\n");
			break;
		      }
		    res = 1;
		    printf("Game créé :)\n");
		    menu1 = 0;
		    break;
	      case 3:
		printf("Très bien on attends alors :)\n");
		sleep(5);
		system("clear");
		break;
	      default:
		printf("Connait pas...");
		fflush(stdout);
		sleep(2);
		system("clear");
	      }
	  }
	free(addr_tile);
	free(addr_game);
	return res;	
}

void print_rule(game g)
{
  int*rules = g.rules;
  printf("\nIl y a plusieurs manières de marquer des points :\n\n");
  printf("Tout d'abord grâce aux villages :\n");
  printf("On regarde le plus grand village (plus grand bloc de cases Ville contigues). Le joueur marque (nombre de case ville de ce village) points. ");
  if (rules[3] == 1)
    {
      printf("De plus si ce village contient un carré de quatre cases village, un bonus de quatre points est attribué.");
    }
  printf("\n\nEnsuite grâce aux forêts :\n");
  if (rules[4] == 0)
    {
      printf("On compte les Forets. Le joueur marque 2*(nombre de cases forêt) points.");
    }
  else
    {
      printf("Pour chaque forêt, la première case vaut 1, la seconde 2, la troisième 3, etc. (max5)"); 
    }
  printf("\n\nOn peut également marquer des points grâce aux lacs :\n");
  if (rules[0] == 0)
    {
      printf("On regarde tout les Lacs. Le joueur marque 3*(nombre de cases Lac du lac - 1) points.");
    }
  else
    {
      printf("Une case Lac vaut deux points.");
    }
  if (rules[1] ==1)
    {
      printf("\n\nUne plaine de quatre cases au moins rapporte quatre points supplémentaires.");
    }
  printf("\n\n Enfin les cases Ressource et Usine permettent de marquer des points :\n");
  if (rules[2] == 0)
    {
      printf("On compte les Ressources. On alloue une Ressource par Usine tant qu’il reste des cases Ressource et des cases Usine. Chaque Ressource alloué à une Usine rapporte quatre points. Une Usine ne peut traiter qu’une Ressource et une Ressource ne peut être allouée qu’à une Usine.");
    }
  else
    {
      printf("On compte les Ressources. On alloue deux Ressource par Usine tant qu’il reste des cases Ressource et des cases Usine. Chaque Ressource alloué à une Usine rapporte quatre points. Une Usine peut traiter une ou deux Ressource et une Ressource ne peut être allouée qu’à une Usine.");
    }
}
