#ifndef SCORE_H
#define SCORE_H
#include "recouvrement.h"
#include <stdio.h>
#include <stdlib.h>

/**
 * \file score.h
 * \brief Définit les fonctions nécessaires au calcul du score
 * \author Atchoum
 * \date Avril
 *
 * Ce fichier définit les fonctions de calcul de score. Il contient 8 fonctions:
 * - \b village(grid,n,x,y,res,visited) qui délimite le village lié à une case ville
 * - \b lac(grid,n,x,y,res,visited) qui délimite le lac lié à une case lac
 * - \b score(grid,n,rules,detail_score) qui calcule le score d'une partie
 * - \b pot(p,rules) qui renvoie le potentiel d'une case
 * - \b potentiel(t,rules) qui détermine le potentiel d'une tuile
 * - \b potentiel_lost(g,t) qui renvoie le potentiel perdu en posant une tuile
 * - \b taille(grid,n,x,y,res,visited) qui délimite une zone de même terrain
 * - \b square(grid,n,zone,size) qui détermine si une zone possède un carré
 */

/**
 * \brief Delimite un village à partir d'une case ville.
 * \details A partir d'une case ville, délimite le village la contenant, stock chacune des cases et renvoie la taille du village.
 * \param grid la grille de jeu, non vide
 * \param n la taille de la grille, un entier positif
 * \param x la coordonnée de la case ville, un entier positif inférieur à n
 * \param y la coordonnée de la case ville, un entier positif inférieur à n
 * \param res un tableau d'entier pour stocker les coordonnées de chacune des cases vides.  
 * \param visited un tableau d'entier pour savoir si une case ville est déjà visitée.
 * \return la taille du village correspondant, un entier positif 
 */
int village(pile** grid, int n, int x, int y, int** res, int** visited);

/**
 * \brief Delimite un lac à partir d'une case lac.
 * \details A partir d'une case lac, délimite le village la contenant, stock chacune des cases et renvoie la taille du village.
 * \param grid la grille de jeu, non vide
 * \param n la taille de la grille, un entier positif
 * \param x la coordonnée de la case ville, un entier positif inférieur à n
 * \param y la coordonnée de la case ville, un entier positif inférieur à n
 * \param res un tableau d'entiers pour stocker les coordonnées de chacune des cases vides.  
 * \param visited un tableau d'entier pour savoir si une case ville est déjà visitée.
 * \return la taille du village correspondant, un entier positif 
 */
int lac(pile** grid, int n, int x, int y, int** res, int** visited);

/**
 * \brief Calcule le score d'une partie
 * \details Calcule le score d'une partie à partir de la grille de jeu.
 * \param grid la grille de jeu, non vide
 * \param n la taille de la grille, un entier positif
 * \param rules Tableau d'entier définissant les règles
 * \param detail_score un tableau d'entiers pour stocker le détail du score
 * \return le score de la partie
 */
int score(pile** grid,int n,int*rules,int* detail_score);

/**
 * \brief Renvoie le potentiel d'une case
 * \details Renvoie le potentiel de la case, c'est-à-dire les points qu'il est possible d'obtenir. Selon les règles la valeur de la case varie.
 * \param p La pile dont on calcule le potentiel du sommet
 * \param rules Les règles de la partie
 * \return le potentiel de la case, un entier positif 
 */
int pot(pile p,int*rules);

/**
 * \brief Renvoie le potentiel d'une tuile
 * \details Renvoie le potentiel de la tuile, c'est-à-dire les points qu'il est possible d'obtenir en fonction des règles de la partie.
 * \param t une tuile
 * \param rules Un tableau d'entier des règles de la partie
 * \return le potentiel de la tuile, un entier positif 
 */
int potentiel(tile t,int*rules);

/**
 * \brief Calcule le potentiel perdu en posant une tuile
 * \details Calcule le potentiel des cases sur lesquelles on souhaite poser une tuile.
 * \param g une partie
 * \param t une tuile
 * \return le potentiel perdu, un entier positif 
 */
int potentiel_lost(game g, tile t);

/**
 * \brief Delimite une zone à partir d'une case.
 * \details A partir d'une case, délimite la zone de même terrain (forêt,lac etc...), stock chacune des cases et renvoie la taille de la zone.
 * \param grid la grille de jeu, non vide
 * \param n la taille de la grille, un entier positif
 * \param x la coordonnée de la case, un entier positif inférieur à n
 * \param y la coordonnée de la case, un entier positif inférieur à n
 * \param res un tableau d'entier pour stocker les coordonnées de chacune des cases vides.  
 * \param visited un tableau d'entier pour savoir si une case est déjà visitée.
 * \return la taille de la zone correspondante, un entier positif 
 */
int taille(pile** grid, int n, int x, int y, int** res, int** visited);

/**
 * \brief Détermine si la case fait partie d'un carré.
 * \details Renvoie true si i,j fait partie d'un carré de même terrain dans grid, et assigne toutes les cases de visited associées à 1. Renvoie false sinon. i,j doit être une case valide dans la grille et n la taille de la grille.
 * \param grid La grille non null
 * \param n La taille de la grille
 * \param zone La zone dans laquelle chercher un carré.
 * \param La taille de cette zone
 * \return Le booléen true si la case fait partie d'un carré de même terrain, false sinon.
 */
int square(pile**grid,int n,int**zone,int size);
#endif
