#ifndef AFF_H
#define AFF_H
#include "init.h"
#include "score.h"

/**
 * \file affichage.h
 * \brief Fonctions d'affichage de la partie
 * \author ByeZoé
 * \date jeudi 8 avril
 *
 * Ce fichier contient les différentes fonctions d'affichage d'une partie. Il contient 5 fonctions:
 * - \b print_game(g) pour afficher une partie
 * - \b select_tile(g,selected_tile,select) pour afficher le menu de sélection
 * - \b rotate_tile(selected_rotation,selected_tile) pour afficher le menu de rotation
 * - \b print_choice(choice) pour afficher le menu de choix
 * - \b start_menu(game) pour afficher le menu d'accueil et de démarrage d'une partie
 * - \b print_rule(game) pour afficher les règles du jeu
 */


/**
 * \brief Afficher une partie
 * \details Affiche l'état actuel d'une partie, c'est-à-dire la liste des tuiles en main ainsi que la grille.
 * \param g Une partie bien paramétrée.
 */
void print_game(game g);

/**
 * \brief Afficher le menu de séléction d'une tuile
 * \details Affiche le menu de sélection d'une tuile, et stock le numéro ainsi qu'une copie de la tuile choisie.
 * \param g une partie.
 * \param selected_tile un pointeur valide vers une tuile pour stocker la tuile choisie.
 * \param select un pointeur valide vers un entier pour stocker le numéro de la tuile choisie dans la liste de la main.
 */
void select_tile(game g,tile* selected_tile, int* select);

/**
 * \brief Afficher le menu de rotation d'une tuile
 * \details Affiche le menu de rotation d'une tuile, et stock la rotation choisie.
 * \param selected_tile un pointeur valide vers la tuile choisie.
 * \param selected_rotation un pointeur valide pour stocker la rotation choisie de la tuile.
 */
void rotate_tile(int* selected_rotation, tile* selected_tile);


/**
 * \brief Afficher le menu des choix
 * \details Affiche les choix de jeu : selectionner une tuile ou bien quitter sont les seuls choix disponibles pour le moment.
 * \param choice un pointeur valide pour stocker la valeur du choix.
 */
void print_choice(int* choice);
/**
 * \brief Afficher le menu de départ.
 * \details Affiche le menu de début de partie. On peut ou bien lancer une partie aléatoire, ou bien lancer une partie à partir d'un fichier.
 * \param g un pointeur vers une game pour stocker la partie créée.
 * \return 0 si il n'y a pas eu de problèmes, 1 sinon.
 */
int start_menu(game *g);

/**
 * \brief Affiche les règles du jeu.
 * \details Affiche les règles selon la valeur de g.rules.
 * \param g La partie en cours
 */
void print_rule(game g);
#endif
