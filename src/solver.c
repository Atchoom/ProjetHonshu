#include "solver.h"
#include "init.h"
#include "affichage.h"
#include "recouvrement.h"
#include "score.h"
#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*la meilleure tuile pour la suite*/
void best_tile(game* g, int* id_res, int* score_res, int* x_res, int* y_res, int* rota_res, int* detail_score)
{
	int i,j,k;	
	/*rotation initiale de la tuile étudiée*/
	int old_rot;
	/*Accumulateur pour vérifier que les règles de pose sont respectées*/
	int rule;
	/*taille de la grille*/
	int n = g->lg;
	/*Taille initiale de la main*/
	int old_hd_lg = g->hd_lg;
	/*Tuile actuelle*/
	tile current_tile;
	/*vecteur coordonnées*/
	int position[2];
	/*Score de la partie actuelle*/
	int curr_score = 0;
	/*Potentiel perdu de la meilleur tuile*/
	int potential_lost = 0;
	/*Potentiel perdu de la tuile actuelle*/
	int current_pot = 0;
	/*liste temporaire pour accumuler les tuiles*/
	list temp = create_list();
	/*On initialise le score a 0*/
	*score_res = 0;
	/*Tant qu'il reste des tuiles dans la main, on continue de chercher la meilleure tuile possible*/
	while(g->hand != NULL)
	{
		/*On prend une tuile pas encore étudiée*/
		current_tile = remove_list(&(g->hand),(g->hd_lg));
		/*On la stock dans la liste temporaire*/
		push_list(&temp,current_tile);
		g->hd_lg -= 1;
		old_rot = current_tile.rotation;
		/*Cas pour une rotation 0*/
		current_tile.rotation = 0;
		/*On parcourt toutes les cases*/
		for(i=0;i<n-2;i++)
		{
			for(j=0;j<n-1;j++)
			{
				/*On stock les coordonnées*/
				position[0] = i;
				position[1] = j;
				/*On vérifie que les différentes regles de pose sont respectés*/
				if(covering_tile_test(*g,current_tile,position)&&covered_lake(g->grid,position,0))
				{
					rule = 0;
					for(k=0;k<length_list(g->posed);k++)
					{
						if(!tile_covered_test(*g,display_list(g->posed,k),position,0))
						{
							rule = 1;
							break;	
						}
					}
					/*Les regles sont respectées, on regarde alors le score*/
					if(rule == 0)
					{
						/*On l'insert*/
						insert_tile(g->grid,g->lg,&current_tile,position);
						curr_score = score(g->grid,n,g->rules,detail_score);
						current_pot = potentiel_lost(*g,current_tile); 
						/*Le score est-il le meilleur?*/
						if(curr_score - current_pot > *score_res - potential_lost)
						{
							/*Si oui, on stock les données*/
							*id_res = g->hd_lg+1;
							*rota_res = current_tile.rotation;
							*x_res = i;
							*y_res = j;
							*score_res = curr_score;
							potential_lost = current_pot;
						}
						/*On enleve la tuile, mais on la remet pas tout de suite dans la main*/
						push_list(&(g->posed),current_tile);
						remove_last_tile(g);
					}
				}
			}
		}
		/* La meme chose, mais avec une rotation de 1, puis de 2 et enfin de 3*/
		current_tile.rotation = 1;
		for(i=0;i<n-1;i++)
		{
			for(j=0;j<n-2;j++)
			{
				position[0] = i;
				position[1] = j;
				if(covering_tile_test(*g,current_tile,position)&&covered_lake(g->grid,position,1))
				{
					rule = 0;
					for(k=0;k<length_list(g->posed);k++)
					{
						if(!tile_covered_test(*g,display_list(g->posed,k),position,1))
						{
							rule = 1;
							break;	
						}
					}
					if(rule == 0)
					{
						insert_tile(g->grid,g->lg,&current_tile,position);
						curr_score = score(g->grid,n,g->rules,detail_score);
						current_pot = potentiel_lost(*g,current_tile); 						
						if(curr_score - current_pot > *score_res - potential_lost)
						{
							*id_res =(g->hd_lg)+1;
							*rota_res = current_tile.rotation;
							*x_res = i;
							*y_res = j;
							*score_res = curr_score;
							potential_lost = current_pot;
						}	
						push_list(&(g->posed),current_tile);
						remove_last_tile(g);
					}
				}
			}
		}
		current_tile.rotation = 2;
		for(i=0;i<n-2;i++)
		{
			for(j=0;j<n-1;j++)
			{
				position[0] = i;
				position[1] = j;
				if(covering_tile_test(*g,current_tile,position)&&covered_lake(g->grid,position,2))
				{
					rule = 0;
					for(k=0;k<length_list(g->posed);k++)
					{
						if(!tile_covered_test(*g,display_list(g->posed,k),position,2))
						{
							rule = 1;
							break;	
						}
					}
					if(rule == 0)
					{
						insert_tile(g->grid,g->lg,&current_tile,position);
						curr_score = score(g->grid,n,g->rules,detail_score);
						current_pot = potentiel_lost(*g,current_tile); 
						if(curr_score - current_pot	> *score_res - potential_lost)
						{
							*id_res = g->hd_lg+1;
							*rota_res = current_tile.rotation;
							*x_res = i;
							*y_res = j;
							*score_res = curr_score;							
							potential_lost = current_pot;
						}	
						push_list(&(g->posed),current_tile);
						remove_last_tile(g);
					}
				}
			}
		}
		current_tile.rotation = 3;
		for(i=0;i<n-1;i++)
		{
			for(j=0;j<n-2;j++)
			{
				position[0] = i;
				position[1] = j;
				if(covering_tile_test(*g,current_tile,position)&&covered_lake(g->grid,position,3))
				{
					rule = 0;
					for(k=0;k<length_list(g->posed);k++)
					{
						if(!tile_covered_test(*g,display_list(g->posed,k),position,3))
						{
							rule = 1;
							break;	
						}
					}
					if(rule == 0)
					{
						insert_tile(g->grid,g->lg,&current_tile,position);
						curr_score = score(g->grid,n,g->rules,detail_score);
						current_pot = potentiel_lost(*g,current_tile);
						if(curr_score - current_pot > *score_res - potential_lost)
						{	
							*id_res =  g->hd_lg+1;
							*rota_res = current_tile.rotation;
							*x_res = i;
							*y_res = j;
							*score_res = curr_score;
							potential_lost = current_pot;
						}
						push_list(&(g->posed),current_tile);
						remove_last_tile(g);
					}
				}
			}
		}
		/*On remet la bonne rotation a la tuile*/
		current_tile.rotation = old_rot;
	}
	/*Une fois la meilleur tuile trouvée, on replace les tuiles dans la main*/
	while(temp != NULL)
	{
		current_tile = remove_list(&temp,length_list(temp));
		push_list(&(g->hand),current_tile);
	}
	g->hd_lg = old_hd_lg;
	free_list(temp);
}

/*Meme fonction mais sans le choix de tuile*/
void best_position(game* g,tile current_tile, int* score_res, int* x_res, int* y_res, int* rota_res, int* detail_score)
{
	int i,j,k;	
	/*rotation initiale de la tuile étudiée*/
	int old_rot;
	/*Accumulateur pour vérifier que les règles de pose sont respectées*/
	int rule;
	/*taille de la grille*/
	int n = g->lg;
	/*Taille initiale de la main*/
	int old_hd_lg = g->hd_lg;
	/*vecteur coordonnées*/
	int position[2];
	/*Score de la partie actuelle*/
	int curr_score = 0;
	/*Potentiel perdu de la meilleur tuile*/
	int potential_lost = 0;
	/*Potentiel perdu de la tuile actuelle*/
	int current_pot = 0;
	/*On initialise le score a 0*/
	*score_res = 0;
	g->hd_lg -= 1;
	old_rot = current_tile.rotation;
	/*Cas pour une rotation 0*/
	current_tile.rotation = 0;
	/*On parcourt toutes les cases*/
	for(i=0;i<n-2;i++)
	{
		for(j=0;j<n-1;j++)
		{
			/*On stock les coordonnées*/
			position[0] = i;
			position[1] = j;
			/*On vérifie que les différentes regles de pose sont respectés*/
			if(covering_tile_test(*g,current_tile,position)&&covered_lake(g->grid,position,0))
			{
				rule = 0;
				for(k=0;k<length_list(g->posed);k++)
				{
					if(!tile_covered_test(*g,display_list(g->posed,k),position,0))
					{
						rule = 1;
						break;	
					}
				}
				/*Les regles sont respectées, on regarde alors le score*/
				if(rule == 0)
				{
					/*On l'insert*/
					insert_tile(g->grid,g->lg,&current_tile,position);
					curr_score = score(g->grid,n,g->rules,detail_score);
					current_pot = potentiel_lost(*g,current_tile); 
					/*Le score est-il le meilleur?*/
					if(curr_score - current_pot > *score_res - potential_lost)
					{
						/*Si oui, on stock les données*/
						*rota_res = current_tile.rotation;
						*x_res = i;
						*y_res = j;
						*score_res = curr_score;
						potential_lost = current_pot;
					}
					/*On enleve la tuile, mais on la remet pas tout de suite dans la main*/
					push_list(&(g->posed),current_tile);
					remove_last_tile(g);
				}
			}
		}
	}
	/* La meme chose, mais avec une rotation de 1, puis de 2 et enfin de 3*/
	current_tile.rotation = 1;
	for(i=0;i<n-1;i++)
	{
		for(j=0;j<n-2;j++)
		{
			position[0] = i;
			position[1] = j;
			if(covering_tile_test(*g,current_tile,position)&&covered_lake(g->grid,position,1))
			{
				rule = 0;
				for(k=0;k<length_list(g->posed);k++)
				{
					if(!tile_covered_test(*g,display_list(g->posed,k),position,1))
					{
						rule = 1;
						break;	
					}
				}
				if(rule == 0)
				{
					insert_tile(g->grid,g->lg,&current_tile,position);
					curr_score = score(g->grid,n,g->rules,detail_score);
					current_pot = potentiel_lost(*g,current_tile); 						
					if(curr_score - current_pot > *score_res - potential_lost)
					{
						*rota_res = current_tile.rotation;
						*x_res = i;
						*y_res = j;
						*score_res = curr_score;
						potential_lost = current_pot;
					}	
					push_list(&(g->posed),current_tile);
					remove_last_tile(g);
					}
				}
			}
		}
	current_tile.rotation = 2;
	for(i=0;i<n-2;i++)
	{
		for(j=0;j<n-1;j++)
		{
			position[0] = i;
			position[1] = j;
			if(covering_tile_test(*g,current_tile,position)&&covered_lake(g->grid,position,2))
			{
				rule = 0;
				for(k=0;k<length_list(g->posed);k++)
				{
					if(!tile_covered_test(*g,display_list(g->posed,k),position,2))
					{
						rule = 1;
						break;	
					}
				}
				if(rule == 0)
				{
					insert_tile(g->grid,g->lg,&current_tile,position);
					curr_score = score(g->grid,n,g->rules,detail_score);
					current_pot = potentiel_lost(*g,current_tile); 
					if(curr_score - current_pot > *score_res - potential_lost)
					{
						*rota_res = current_tile.rotation;
						*x_res = i;
						*y_res = j;
						*score_res = curr_score;							
						potential_lost = current_pot;
					}	
					push_list(&(g->posed),current_tile);
					remove_last_tile(g);
				}
			}
		}
	}
	current_tile.rotation = 3;
	for(i=0;i<n-1;i++)
	{
		for(j=0;j<n-2;j++)
		{
			position[0] = i;
			position[1] = j;
			if(covering_tile_test(*g,current_tile,position)&&covered_lake(g->grid,position,3))
			{
				rule = 0;
				for(k=0;k<length_list(g->posed);k++)
				{
					if(!tile_covered_test(*g,display_list(g->posed,k),position,3))
					{
						rule = 1;
						break;	
					}
				}
				if(rule == 0)
				{
					insert_tile(g->grid,g->lg,&current_tile,position);
					curr_score = score(g->grid,n,g->rules,detail_score);
					current_pot = potentiel_lost(*g,current_tile);
					if(curr_score - current_pot > *score_res - potential_lost)
					{	
						*rota_res = current_tile.rotation;
						*x_res = i;
						*y_res = j;
						*score_res = curr_score;
						potential_lost = current_pot;
					}
					push_list(&(g->posed),current_tile);
					remove_last_tile(g);
				}
			}
		}
	}
	/*On remet la bonne rotation a la tuile*/
	current_tile.rotation = old_rot;
	g->hd_lg = old_hd_lg;
}

void insert_potentiel (game g,list* res, tile t)
{
	tile current_tile;
	int potentiel_t = potentiel(t,g.rules);
	int i = 0;
	for( i = 0; i<length_list(*res);i++)
	{
		current_tile = display_list(*res,i);
		if (potentiel(current_tile,g.rules)<=potentiel_t)
		{
			add_list(res,t,i);
			exit(0);
		}
	}
	add_list(res,t,length_list(*res));
}

void tri_insert_potentiel(game g,list *res, list l)
{
	int i = 0;
	tile current_tile;
	for(i = 0; i<length_list(l); i++)
	{
		current_tile = display_list(l,i+1);
		insert_potentiel(g,res,current_tile);
	}
}

void solver_glo(game* g)
{
	/*Données de la meilleure tuile*/
	int id = 0;
	int score_res = 0;
	int rota = 0;
	int position[2];
	tile selected_tile;
	int* detail_score = (int*)malloc(4*sizeof(int));
	/*Tant que toutes les tuiles ne sont pas posées, on cherche la meilleure possible*/
	while(g->hd_lg != 0)
	{
		/*On cherche la meileur tuile*/
		best_tile(g,&id,&score_res,&position[0],&position[1],&rota,detail_score);
		/*on la pose*/
		selected_tile = remove_list(&(g->hand),id );
		g->hd_lg -= 1;
		selected_tile.rotation = rota;
		insert_tile(g->grid,g->lg,&selected_tile,position);
		push_list(&(g->posed),selected_tile);
	}
	free(detail_score);
}

void solver_glo_pot(game* g)
{
	/*Données de la meilleure tuile*/
	int score_res = 0;
	int rota = 0;
	int position[2];
	tile selected_tile;
	int* detail_score = (int*)malloc(4*sizeof(int));
	list temp= create_list();
	tri_insert_potentiel(*g,&temp,g->hand);
	g->hand = temp;
	/*Tant que toutes les tuiles ne sont pas posées, on cherche la meilleure possible*/
	while(g->hd_lg != 0)
	{
		selected_tile = remove_list(&(g->hand),0);	
		best_position(g,selected_tile,&score_res,&position[0],&position[1],&rota,detail_score);
		/*on la pose*/
		g->hd_lg -= 1;
		selected_tile.rotation = rota;
		insert_tile(g->grid,g->lg,&selected_tile,position);
		push_list(&(g->posed),selected_tile);
	}
	free(detail_score);
}
