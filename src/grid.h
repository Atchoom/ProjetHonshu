#ifndef GRID_H
#define GRID_H
#include "pile.h"

/**
 * \file grid.h
 * \brief Définit les tuiles.
 * \author ByeZoé
 * \date jeudi 22 mars
 *
 * Ce fichier définit la structure des tuiles et les fonctions pour les manipuler. Il contient 2 types et 8 fonctions:
 * - le type \a tile qui définit une tuile
 * - le type \a tuple contenant deux entiers
 * - \b create_playmap(size) pour créer une grille de taille size
 * - \b free_grid(grid,size) pour détruire une grille de taille size
 * - \b create_tile(fields,id) pour créer une tuile avec les terrains de fields 
 * - \b rotate(tile) pour effectuer une rotation d'une tuile
 * - \b print_tile(tile) pour afficher les terrains d'une tuile
 * - \b create_element(tile,num) pour créer un \a element à partir de la tuile
 * - \b insert_tile(playmap,size,tile,position) pour insérer une tuile dans une grille
 * - \b remove_tile(grid,size,id) pour retirer une tuile de la grille
 */

/**
 * \struct tile
 * \brief Structure d'une tuile
 */
typedef struct tile {
	int id;
  /**< Un entier pur l'identifiant de la tuile*/
	int rotation;
  /**< Un entier de 1 à 4 précisant l'orientation de la tuile */
	int position[2];
  /**< Un couple d'entiers positifs précisant la position de la tuile dans le jeu */
	char fields[6];
  /**< Six caractères précisant les terrains formants la tuile */
} tile;

/**
 * \struct tuple
 * \brief couple composé d'un entier (int1) et d'un autre (int2)
 */

typedef struct tuple {
	int int1;
	int int2;
} tuple; 

/** 
 * \brief Crée une grille de jeu.
 * \details Alloue la mémoire pour une grille de jeu pouvant contenir des piles d'\a elements.
 * \param size Un entier positif non nul
 * \return Une grille de taille \a size
 */
pile** create_playmap(int size);

/** 
 * \brief Détruit une grille de jeu.
 * \details Libère la mémoire de toutes les piles d'une grille passée en paramètre.
 * \param grid Une grille carrée à détruire
 * \param size La taille de la grille
 * \attention \a grid doit être de taille \a size
 */
void free_grid(pile** grid, int size);

/** 
 * \brief Crée une tuile.
 * \details Libère Crée une tuile avec les terrains de \a fields passé en paramètre et initialise la rotation et la position de la tuile.
 * \param fields Des terrains pour la tuile
 * \param id Identifiant de la tuile, doit être unique dans la partie
 * \return Une tuile avec les terrains passés en paramètre
 * \attention Les terrains de la tuile doivent être parmi F,L,V,R,U,P
 */
tile create_tile (char fields[6], int id);

/** 
 * \brief Tourne une tuile de 90 degrés.
 * \details Modifie la valeur du paramètre rotation de la tuile pour préciser une orientation différente de 90 degrés dans le sens trigonométrique.
 * \param tile Un pointeur vers la tuile à tourner
 */
void rotate(tile*tile);

/** 
 * \brief Affiche une tuile.
 * \details Affiche tous les terrains de la tuile en fonction de sa position.
 * \param t Une tuile à afficher
 */
void print_tile(tile t);

/** 
 * \brief Crée un \a element.
 * \details Crée un \a element à partir de la tuile en lui associant l'id de la tuile et le terrain.
 * \param tile Une tuile
 * \param num Un entier pour déterminer quel terrain choisir parmi les six
 */
element create_element(tile tile, int num);

/** 
 * \brief Insère une tuile dans une grille.
 * \details Rajoute les six \a elements définissant la tuile dans les piles de la grille et actualise la position de la tuile.
 * \param playmap Une grille
 * \param size La taille de la grille
 * \param tile Un pointeur vers la tuile à insérer
 * \param position Un couple d'entier désignant les coordonnées où insérer la tuile
 * \return Un entier, 1 si la tuile n'a pas pu être insérée, 0 sinon
 * \attention playmap doit être de taille size
 * \attention La tuile doit pouvoir être placée dans la grille, pas de débordement.
 */
int insert_tile(pile** playmap,int size,tile*tile,int position[2]);

/**
 * \brief Retire une tuile
 * \details Retire une tuile de la grille et renvoie true si la tuile a été retirée, false sinon
 * \param grid : Une grille de jeu (carrée)
 * \param size : La taille de ladite grille
 * \param id : Le numéro de la tuile que l'on souhaite retirer
 * \return 0 si la tuile n'a pas pu être retirée, 1 sinon
*/
int remove_tile(pile** grid, int size, int id);

#endif
