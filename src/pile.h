#ifndef PILE_H
#define PILE_H

#include <stdlib.h>
#include <stdio.h>

/**
 * \file pile.h
 * \brief Définit les piles de terrains
 * \author ByeZoé
 * \date jeudi 22 mars
 *
 * Ce fichier définit les piles de terrains et les fonctions usuelles des piles. Il contient 3 types et 9 fonctions:
 * - le type \a element définissant un terrain et sa tuile
 * - le type \a pile définissant une pile d'élément
 * - le type \a square définissant un maillon d'une pile
 * - \b create_pile() pour créer une pile vide
 * - \b push_pile(&p,elt) pour ajouter un élément en tete de la pile
 * - \b pop_pile(&p) pour extraire un l'élément en tete
 * - \b print_pile(p) pour afficher les éléments d'une pile
 * - \b print_top_pile(p) pour afficher le sommet d'une pile
 * - \b free_pile(p) pour détruire une pile et libérer la mémoire
 * - \b peak_pile(p) pour renvoyer le sommet d'une pile
 * - \b peak_last_pile(p) pour renvoyer la base d'une pile
 * - \b empty_pile(p) pour tester si une pile est vide
 */

/**
 * \struct element
 * \brief Définit un terrain et sa tuile
 * \details Un \a element est un terrain et le numéro de la tuile à laquelle il appartient
 */
typedef struct element {
  char field;
  /**< Le type de terrain de l'élément */
  int tile;
  /**< Le numéro de la tuile à laquelle appartient le terrain */
} element;


/**
 * \struct square
 * \brief Composant d'une pile
 * \details Structure représentant le maillon d'une pile, contenant une data (element), et un pointeur vers la suite de la pile
*/
struct square {
	element data;
	/**< Un element */
	struct square* next;
	/**< Pointeur vers le maillon suivant */
};

/**
 * \typedef pile
 * \brief Type définissant les piles d'éléments
 */
typedef struct square* pile;


/**
 * \brief Crée une pile vide.
 * \details Renvoie une pile vide. 
 * \return Une pile vide
 */
pile create_pile();

/**
 * \brief Empiler un élément.
 * \details Alloue la mémoire et rajoute un élément en tête de la pile
 * \param p Un pointeur vers une pile
 * \param elt L'élément à rajouter
 */
void push_pile(pile *p, element elt);

/**
 * \brief Extrait l'élément en tête.
 * \details Retire l'élément de la pile et le renvoie. La pile ne doit pas être vide.
 * \param p Une pile d'élément non vide
 * \return L'élément en tête de la pile
 * \attention La pile ne doit pas être vide
 */
element pop_pile(pile *p);

/**
 * \brief Affiche les terrains d'une pile.
 * \details Affiche à la suite les terrains de la pile. N'affiche rien si la pile est vide.
 * \param p Une pile d'éléments
 */
void print_pile(pile p);

/**
 * \brief Affiche le sommet de la pile.
 * \details Affiche le sommet de l'élément en tête de la pile ou "*" si elle est vide.
 * \param Une pile d'éléments
 * \return Le terrain de l'élément en tête de pile, "*" si elle est vide
 */
void print_top_pile(pile p);

/**
 * \brief Détruis une pile.
 * \details Libère toute la mémoire occupée par une pile.
 * \param Une pile à détruire
 */
void free_pile(pile p);

/**
 * \brief Renvoie le sommet.
 * \details Renvoie l'élément au sommet de la pile sans le retirer de la pile. Elle doit être non vide.
 * \param p Une pile d'élément non vide
 * \return L'élément sommet de la pile
 * \attention La pile ne doit pas être vide
 */
element peak_pile(pile p);

/**
 * \brief Renvoie l'élément le plus enfouie.
 * \details Parcourt la pile jusqu'à sa base pour renvoyer le premier élément empilé. La liste ne doit pas être vide.
 * \param p Une pile d'élément non vide
 * \return L'élément à la base de la pile
 * \attention La pile ne doit pas être vide.
 */
element peak_last_pile(pile p);

/**
 * \brief Teste si la pile est vide.
 * \param p Une pile
 * \return true si la pile est vide, false sinon
 */
int empty_pile(pile p);
#endif
