#include <stdio.h>
#include <string.h>
#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"
#include "../src/pile.h"
#include "../src/list.h"
#include "../src/grid.h"
#include "../src/init.h"
#include "../src/recouvrement.h"

/*Necessaire pour le registre*/
static FILE* temp_file = NULL;

int init_suite(void)
{
   if (NULL == (temp_file = fopen("temp.txt", "w+"))) {
      return -1;
   }
   else {
      return 0;
   }
}

int clean_suite(void)
{		
   if (0 != fclose(temp_file)) {
      return -1;
   }
   else {
      temp_file = NULL;
      return 0;
   }
}


/*Tests pile*/

element el_1 = {.field = 'A', .tile = 1};


int equals_elt(element a, element b)
{
	return ((a.field == b.field) && (a.tile == b.tile));
}

void test_create_pile(void)
{
	CU_ASSERT_FALSE(create_pile());
}

void test_empty_pile(void)
{
  pile p = create_pile();
  CU_ASSERT_TRUE(empty_pile(p));
  push_pile(&p,el_1);
  CU_ASSERT_FALSE(empty_pile(p));
  free_pile(p);
}

void test_push_pile(void)
{
	pile p = create_pile();
	push_pile(&p,el_1);
	CU_ASSERT_NOT_EQUAL(equals_elt(p->data,el_1),0);
	free_pile(p);
}

void test_pop_pile(void)
{
	pile p = create_pile();
	push_pile(&p,el_1);
	CU_ASSERT_NOT_EQUAL(equals_elt(pop_pile(&p),el_1),0);
	CU_ASSERT_FALSE(p);
	free_pile(p);

}

void test_peak_pile(void)
{
  pile p = create_pile();
	push_pile(&p,el_1);
	CU_ASSERT_NOT_EQUAL(equals_elt(peak_pile(p),el_1),0);
	free_pile(p);
}

void test_peak_last_pile(void)
{
  element el_1 = {.field = 'A', .tile = 1};
  element el_2 = {.field = 'A', .tile = 2};
  pile p = create_pile();
  push_pile(&p,el_1);
  push_pile(&p,el_2);
  CU_ASSERT_NOT_EQUAL(equals_elt(peak_last_pile(p),el_1),0);
  element el_3 = {.field = 'A', .tile = 3};
  push_pile(&p,el_3);
  CU_ASSERT_NOT_EQUAL(equals_elt(peak_last_pile(p),el_1),0);
  free_pile(p);
}

/*Tests List */

tile t1 = {.id = 1, .rotation = 0, .position = {0,0}, .fields = "ABCDEF"};
tile t2 = {.id = 2, .rotation = 0, .position = {1,1}, .fields = "ACBDEF"};

int equals_tile(tile a,tile b)
{
	int res = 1;
	int i=0;
	for(i=0;i<6;i++)
	{
		if(!(a.fields[i] == b.fields[i]))
		{
			res = 0;
		}
	}
	return ((a.rotation == b.rotation) && (a.position[1] == b.position[1]) && (a.position[0] == b.position[0])&&res);
}

void test_create_list(void)
{	
	CU_ASSERT_FALSE(create_list());
}

void test_push_list(void)
{
	list l = create_list();
	push_list(&l,t1);
	CU_ASSERT_NOT_EQUAL(equals_tile(l->data,t1),0);
	free_list(l);
}

void test_remove_list(void)
{
	list l = create_list();
	push_list(&l,t1);
	push_list(&l,t2);
	CU_ASSERT_NOT_EQUAL(equals_tile(remove_list(&l,2),t1),0);
	CU_ASSERT_NOT_EQUAL(equals_tile(remove_list(&l,1),t2),0);
	CU_ASSERT_TRUE(l==NULL);
	free_list(l);	
}

void test_display_list(void)
{
	list l = create_list();
	list l2 = create_list();
	push_list(&l,t1);
	push_list(&l,t2);
	push_list(&l2,t1);
	push_list(&l2,t2);
	CU_ASSERT_NOT_EQUAL(equals_tile(display_list(l,2),t1),0);
	CU_ASSERT_NOT_EQUAL(equals_tile(display_list(l,1),t2),0);
	CU_ASSERT_NOT_EQUAL(equals_tile(l->data,l2->data),0);
	CU_ASSERT_NOT_EQUAL(equals_tile((l->next)->data,(l2->next)->data),0);	
	free_list(l);
	free_list(l2);
}

void test_length_list(void)
{
	list l = create_list();
	CU_ASSERT_EQUAL(length_list(l),0);
	push_list(&l,t1);
	push_list(&l,t2);
	CU_ASSERT_EQUAL(length_list(l),2);
	free_list(l);
}

void test_copy_list(void)
{
  list l = create_list();
  push_list(&l,t1);
  push_list(&l,t2);
  list l2 = copy_list(l,2);
  CU_ASSERT_NOT_EQUAL(equals_tile(l->data,l2->data),0);
  CU_ASSERT_NOT_EQUAL(equals_tile((l->next)->data,(l2->next)->data),0);
  free_list(l);
  free_list(l2);
}
/* Tests pour les grilles */

void test_create_tile(void)
{
  char test_field[6] = "PURLFU";
  tile t = {.id = 1, .rotation = 0, .position = {-1,-1}, .fields = "PURLFU"};
  tile test_tile = create_tile(test_field,1);
  CU_ASSERT_TRUE(equals_tile(test_tile,t));
}

void test_rotate(void)
{
  tile t = {.id = 1,.rotation = 0, .position = {-1,-1}, .fields = "PURLFU"};
  tile t_rot = {.id = 1, .rotation = 1, .position = {-1,-1}, .fields = "PURLFU"};
  tile t_mod = {.id = 1, .rotation = 3, .position = {-1,-1}, .fields = "PURLFU"};
  tile tb = {.id = 1, .rotation = 0, .position = {-1,-1}, .fields = "PURLFU"};
  rotate(&t);
  rotate(&t_mod);
  CU_ASSERT_TRUE(t.rotation==t_rot.rotation);
  CU_ASSERT_TRUE(t_mod.rotation==tb.rotation);
}

void test_create_element(void)
{
  element elt = {.field = 'R', .tile = 2};
  tile t = {.id = 2, .rotation = 0, .position = {-1,-1}, .fields = "PURLFU"};
  element test_elt = create_element(t,2);
  CU_ASSERT_TRUE((elt.field == test_elt.field) && (elt.tile == test_elt.tile));
}

void test_insert_tile(void)
{
  tile t = {.id = 2, .rotation = 0, .position = {-1,-1}, .fields = "PURLFU"};
  tile t2 = {.id = 3, .rotation = 1, .position = {-1,-1}, .fields = "PURLFU"};
  pile** play_test = create_playmap(3);
  int position[2] = {0,0};
  int position2[2] = {2,0};
  insert_tile(play_test,3,&t,position);
  CU_ASSERT_TRUE((((play_test[0][0])->data).field == 'P') && (((play_test[0][1])->data).field == 'U') && 
  	(((play_test[1][0])->data).field == 'R') && (((play_test[1][1])->data).field == 'L') &&
  	 (((play_test[2][0])->data).field == 'F') && (((play_test[2][1])->data).field == 'U') &&
  	  (((play_test[0][0])->data).tile == 2) && (((play_test[0][1])->data).tile == 2) &&
  	   (((play_test[1][0])->data).tile == 2) && (((play_test[1][1])->data).tile == 2) &&
  	    (((play_test[2][0])->data).tile == 2) && (((play_test[2][1])->data).tile == 2) &&
 			(t.position[0] == 0) && (t.position[1] == 0));
  insert_tile(play_test,3,&t2,position);
  CU_ASSERT_TRUE((((play_test[0][0])->data).field == 'F') && (((play_test[0][1])->data).field == 'R') && 
  	(((play_test[0][2])->data).field == 'P') && (((play_test[1][0])->data).field == 'U') &&
  	 (((play_test[1][1])->data).field == 'L') && (((play_test[1][2])->data).field == 'U') &&
  	  (((play_test[0][0])->data).tile == 3) && (((play_test[0][1])->data).tile == 3) &&
  	   (((play_test[0][2])->data).tile == 3) && (((play_test[1][0])->data).tile == 3) &&
  	    (((play_test[1][1])->data).tile == 3) && (((play_test[1][2])->data).tile == 3) &&
 		(t2.position[0] == 0) &&(t2.position[1] == 0) && (((play_test[2][0])->data).field == 'F') &&
  	       (((play_test[2][1])->data).field == 'U'));
  CU_ASSERT_EQUAL(insert_tile(play_test,3,&t,position2),1);
  free_grid(play_test,3);	
}

/*Tests init */

void test_random_rule(void)
{
  int*a=random_rule(5,3);
  int count = 0;
  int i;
  for (i=0;i<5;i++)
    {
      count+=a[i];
    }
  CU_ASSERT_EQUAL(count,3);
  free(a);
}

void test_in_tab(void)
{
	int *tab = (int*)malloc(3*sizeof(int));
	tab[0] = 1;
	tab[1] = 2;
	tab[2] = 3;	
	CU_ASSERT_EQUAL(in_tab(tab,2,3),1);
	CU_ASSERT_EQUAL(in_tab(tab,5,3),0);
	free(tab);
}

void test_create_files(void)
{
	tile tile1 = create_tile("ABCDEF",0);
	tile tile2 = create_tile("GHIJKL",1);
	tile tile3 = create_tile("MNOPQR",2);	
	int* hand = malloc(2*sizeof(int));
	hand[0] = 1;
	hand[1] = 3;
	list tiles = create_list();
	push_list(&tiles,tile1);
	push_list(&tiles,tile2);
	push_list(&tiles,tile3);
	CU_ASSERT_EQUAL(create_files(10,3,hand,2,2,tiles),0);
	free(hand);
}

void test_init_game_f(void)
{
	tile tile1 = create_tile("ABCDEF",0);
	tile tile2 = create_tile("GHIJKL",1);
	tile tile3 = create_tile("MNOPQR",2);	
	int* hand = malloc(2*sizeof(int));
	hand[0] = 1;
	hand[1] = 3;
	list tiles = create_list();
	push_list(&tiles,tile1);
	push_list(&tiles,tile2);
	push_list(&tiles,tile3);
	create_files(10,3,hand,2,2,tiles);
	game g;
	CU_ASSERT_EQUAL(init_game_f(&g,"file_tiles.txt","file_game.txt"),0);
	free(hand);
	free_grid(g.grid,g.lg);
	free_list(g.hand);
	free_list(g.posed);
}

/* Test de tests de recouvrements */
void test_tile_covered_test(void)
{
	pile** grid = create_playmap(3);
	int lg = 3;
  	int hd_lg = 0;
  	list hand = create_list();
  	list posed = create_list();
  	game g = {.lg = lg, .hd_lg = hd_lg, .grid = grid, .hand = hand, .posed = posed};
  	int position[2] = {0,0};
  	int position2[2] = {5,5};
  	tile t0 = {.rotation = 0, .position = {-1,-1}, .fields = "PURLFU", .id = 0};
  	tile t1 = {.rotation = 0, .position = {-1,-1}, .fields = "UFLRUP", .id = 1};
  	tile t2 = {.rotation = 0, .position = {-1,-1}, .fields = "PURLFU", .id = 2};
  	insert_tile(grid,3,&t0,position);
  	CU_ASSERT_TRUE(tile_covered_test(g,t1,position2,1));
  	insert_tile(grid,3,&t1,position);
  	CU_ASSERT_FALSE(tile_covered_test(g,t0,position2,1));
  	CU_ASSERT_TRUE(tile_covered_test(g,t2,position2,1));
  	free_grid(grid,3);
  	free_list(hand);
  	free_list(posed);
}

/* t1 et t2 nécessairement de même taille sz */
/* compare les tableaux case à case */
int compare_tab_tuple(tuple*t1,tuple*t2, int sz)
{
  int res = 1;
  int i=0;
  while (i<sz && res)
    {
      res = res && ((t1[i]).int1 == (t2[i]).int1) && ((t1[i]).int1 == (t2[i]).int1);
      i++;
    }
  return res;
}
  
void test_square_covered(void)
{
  pile** grid = create_playmap(3);
  int lg = 3;
  int hd_lg = 0;
  list hand = create_list();
  list posed = create_list();
  game g = {.lg = lg, .hd_lg = hd_lg, .grid = grid, .hand = hand, .posed = posed};
  tile t0 = {.rotation = 0, .position = {-1,-1}, .fields = "PURLFU", .id = 0};
  tile t1 = {.rotation = 1, .position = {-1,-1}, .fields = "UFLRUP", .id = 1};
  int position0[2] = {0,0};
  int position1[2] = {1,0};
  tuple position00; position00.int1=0; position00.int2=0;
  tuple position01; position01.int1=0; position01.int2=1;
  tuple position10; position10.int1=1; position10.int2=0;
  tuple position11; position11.int1=1; position11.int2=1;
  tuple position12; position12.int1=1; position12.int2=2;
  tuple position20; position20.int1=2; position20.int2=0;
  tuple position21; position21.int1=2; position21.int2=1;
  tuple position22; position22.int1=2; position22.int2=2;
  int nb_covered;
  tuple*covered=square_covered(g,&nb_covered);
  tuple*test=calloc(0,sizeof(tuple));
  CU_ASSERT_TRUE(nb_covered == 0);
  CU_ASSERT_TRUE(compare_tab_tuple(covered,test,0));
  insert_tile(g.grid,g.lg,&t0,position0);
  /* on libère les tableaux pour recommencer */
  free(covered);
  free(test);
  covered=square_covered(g,&nb_covered);
  test=calloc(6,sizeof(tuple)); /* on crée ce qu'on veut observer à la main */
  test[0] = position00;
  test[1] = position01;
  test[2] = position10;
  test[3] = position11;
  test[4] = position20;
  test[5] = position21;
  CU_ASSERT_TRUE(nb_covered == 6);
  CU_ASSERT_TRUE(compare_tab_tuple(covered,test,6));
  free(covered);
  free(test);
  insert_tile(g.grid,g.lg,&t1,position1);
  covered=square_covered(g,&nb_covered);
  test=calloc(8,sizeof(tuple)); /* on crée ce qu'on veut observer à la main */
  test[0] = position00;
  test[1] = position01;
  test[2] = position10;
  test[3] = position11;
  test[4] = position12;
  test[5] = position20;
  test[6] = position21;
  test[7] = position22;
  CU_ASSERT_TRUE(nb_covered == 8);
  CU_ASSERT_TRUE(compare_tab_tuple(covered,test,8));
  free(covered);
  free(test);
  free_grid(g.grid,g.lg);
  free_list(g.hand);
  free_list(g.posed);
}

void test_covering_tile_test(void)
{
  pile** grid = create_playmap(5);
  int lg = 5;
  int hd_lg = 0;
  list hand = create_list();
  list posed = create_list();
  game g = {.lg = lg, .hd_lg = hd_lg, .grid = grid, .hand = hand, .posed = posed};
  tile t0 = {.rotation = 0, .position = {-1,-1}, .fields = "PURLFU", .id = 0};
  tile t1 = {.rotation = 1, .position = {-1,-1}, .fields = "UFLRUP", .id = 1};
  tile t2 = {.rotation = 0, .position = {-1,-1}, .fields = "PURLFU", .id = 2};
  tile t3 = {.rotation = 0, .position = {-1,-1}, .fields = "PURLFU", .id = 3};
  int position0[2] = {0,0};
  int position1[2] = {1,0};
  int position2[2] = {2,2};
  int position3[2] = {0,2};
  CU_ASSERT_FALSE(covering_tile_test(g,t1,position1)); /* t1 ne repose sur rien */
  insert_tile(g.grid,g.lg,&t0,position0); /* on rajoute une base à notre grille */
  CU_ASSERT_TRUE(covering_tile_test(g,t1,position1)); /* t1 repose sur t0 maintenant */
  CU_ASSERT_FALSE(covering_tile_test(g,t3,position3)); /* t3 ne repose sur rien */
  insert_tile(g.grid,g.lg,&t1,position1); /* on insère t1 */
  CU_ASSERT_TRUE(covering_tile_test(g,t2,position2)); /* t2 repose sur t1 qui repose sur t0 */
  insert_tile(g.grid,g.lg,&t2,position2); /* on insère t2 */
  free_grid(grid,5);
  free_list(hand);
  free_list(posed);
}
/*
  En deux schémas séparés on a :
0===3===    
|A B|C D|E    A B C D E
|   |   |    1=====
|F G|H|I|J   |F G H|I J
|   |   |    |   2=|=
|K L|M N|O   |K L|M|N|O
 =======      ===|=  |
 P Q R S T    P Q|R S|T
                 |   |
 U V W X Y    U V|W X|Y 
                  ===
*/

void test_covered_squares(void)
{
	int i;
    tile t={.rotation=0, .position={-1,-1}, .fields="PURFLU", .id=0};
	int pos[2]={0, 0};
	int expected[6][2]={{0, 0}, {1, 0}, {2, 0}, {0, 1}, {1, 1}, {2, 1}};
	int** got=covered_squares(t, pos);
	for (i=0;i<6;i++){
	    CU_ASSERT_EQUAL(got[i][0], expected[i][0]);
	    CU_ASSERT_EQUAL(got[i][1], expected[i][1]);
	}
	for(i=0;i<6;i++)
	{
		free(got[i]);
	}
	free(got);
}

void test_covered_lake(void)
{
    pile** grid=create_playmap(5);
	tile t={.rotation=0, .position={-1,-1}, .fields="PURFLU", .id=0};
	int pos[2]={0, 0};
	insert_tile(grid, 5, &t, pos);
	CU_ASSERT_FALSE(covered_lake(grid, pos, 0));
	free_grid(grid, 5);
}

void test_can_insert(void)
{
    int position[2] ={1,0};
    CU_ASSERT_TRUE(can_insert(1,3,position)==1);
    CU_ASSERT_TRUE(can_insert(0,3,position)==0);
}
void test_game_over(void)
{
  pile** grid = create_playmap(5);
  int lg = 5;
  int hd_lg = 0;
  list hand = create_list();
  list posed = create_list();
  game g = {.lg = lg, .hd_lg = hd_lg, .grid = grid, .hand = hand, .posed = posed};
  tile t0 = {.rotation = 0, .position = {-1,-1}, .fields = "PURLFU", .id = 0};
  CU_ASSERT_TRUE(game_over(g));
  push_list(&(g.hand),t0);
  g.hd_lg ++;
  CU_ASSERT_FALSE(game_over(g));
  remove_list(&(g.hand),0);
  g.hd_lg --;
  CU_ASSERT_TRUE(game_over(g));
  free_grid(g.grid,g.lg);
  free_list(g.hand);
  free_list(g.posed);
}

int main()
{
	CU_pSuite pSuite_Pile = NULL;
	CU_pSuite pSuite_List = NULL;
	CU_pSuite pSuite_grid = NULL;
	CU_pSuite pSuite_init = NULL;
	CU_pSuite pSuite_recouvrement = NULL;

	/*Initialisation du registre*/
   	if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

  	/*Ajout d'une suite au registre*/
  	pSuite_Pile = CU_add_suite("Suite_pile",init_suite,clean_suite);
  	pSuite_List = CU_add_suite("Suite_List",init_suite,clean_suite);	
	pSuite_grid = CU_add_suite("Suite_grid",init_suite,clean_suite);
	pSuite_init = CU_add_suite("Suite_init",init_suite,clean_suite);
	pSuite_recouvrement = CU_add_suite("Suite_recouvrement",init_suite,clean_suite);

	if (NULL == pSuite_Pile) {
	    CU_cleanup_registry();
	    return CU_get_error();
	}
	if (NULL == pSuite_List) {
	    CU_cleanup_registry();
	    return CU_get_error();
	}
	if (NULL == pSuite_grid) {
	    CU_cleanup_registry();
	    return CU_get_error();
	}
	if (NULL == pSuite_init) {
	    CU_cleanup_registry();
	    return CU_get_error();
	}
	if (NULL == pSuite_recouvrement) {
	    CU_cleanup_registry();
	    return CU_get_error();
	}

	/*Ajout des tests*/	
	if((NULL == CU_add_test(pSuite_Pile, "Test creation de pile",test_create_pile)) ||
	   (NULL == CU_add_test(pSuite_Pile, "Test de vacuité de pile",test_empty_pile))||
	   (NULL == CU_add_test(pSuite_Pile, "Test ajouter à une pile", test_push_pile)) ||
	   (NULL == CU_add_test(pSuite_Pile, "Test retirer tête d'une pile", test_pop_pile)) ||
	   (NULL == CU_add_test(pSuite_Pile, "Test renvoie de sommet", test_peak_pile))
	   ||
	   (NULL == CU_add_test(pSuite_Pile, "Test renvoie de base", test_peak_last_pile)))
	{
		CU_cleanup_registry();
		return CU_get_error();
	}

	if((NULL == CU_add_test(pSuite_List, "Test creation de liste",test_create_list)) ||
	   (NULL == CU_add_test(pSuite_List, "Test ajouter à une liste", test_push_list)) ||
	   (NULL == CU_add_test(pSuite_List, "Test retirer élement d'une liste", test_remove_list)) ||
	   (NULL == CU_add_test(pSuite_List, "Test afficher element d'une liste", test_display_list)) ||
	   (NULL == CU_add_test(pSuite_List, "Test taille d'une liste", test_length_list)) ||
	   (NULL == CU_add_test(pSuite_List, "Test de copy d'une liste", test_copy_list)))
	{
		CU_cleanup_registry();
		return CU_get_error();
	}
	
	if ((NULL == CU_add_test(pSuite_grid, "Test creation tuile",test_create_tile)) ||
	    (NULL == CU_add_test(pSuite_grid, "Test rotation tuile",test_rotate)) ||
	    (NULL == CU_add_test(pSuite_grid, "Test creation element",test_create_element)) ||
	    (NULL == CU_add_test(pSuite_grid, "Test insertion tuile",test_insert_tile)))
	  {
	    CU_cleanup_registry();
	    return CU_get_error();
	  }
	
	if((NULL == CU_add_test(pSuite_init, "Test de in_Tab",test_in_tab)) ||
	   (NULL == CU_add_test(pSuite_init, "Test de la creation de files", test_create_files)) ||
	   (NULL == CU_add_test(pSuite_init, "Test de la creation de partie a partir de fichiers",test_init_game_f)))
	  {
	    CU_cleanup_registry();
	    return CU_get_error();
	  }

	if ((NULL == CU_add_test(pSuite_recouvrement, "Test de recouvrement d'une tuile", test_tile_covered_test)) ||
	   	(NULL == CU_add_test(pSuite_recouvrement, "Test des terrains recouverts", test_square_covered)) ||
	    (NULL == CU_add_test(pSuite_recouvrement, "Test du support d'une tuile", test_covering_tile_test)) ||
	    (NULL == CU_add_test(pSuite_recouvrement, "Test des terrains recouverts par une tuile", test_covered_squares)) ||
	    (NULL == CU_add_test(pSuite_recouvrement, "Test de lac recouvert", test_covered_lake)) ||
	    (NULL == CU_add_test(pSuite_recouvrement, "Test de fin de partie", test_game_over)) ||
      (NULL == CU_add_test(pSuite_recouvrement, "Test de bordure", test_can_insert)))
	  {
	    CU_cleanup_registry();
	    return CU_get_error();
	  }


	/*Run des test*/
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
