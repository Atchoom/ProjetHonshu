
CC = gcc -Wall -Wextra -ansi -g -lm
CCGTK = gcc
DOCDIR = doc
SRCDIR = src
OBJDIR = obj
BINDIR = bin
TESTDIR = tests
FILEDIR = file
all:
	@echo "Le Makefile propose cinq commandes";
	@echo "- compile : compile l'ensemble des fichiers sources pour exécuter le fichier main.c et crée son exécutable exec dans le dossier bin";
	@echo "- test : compile l'ensemble des fichiers sources pour exécuter les tests unitaires et crée l'exécutable test";
	@echo "- debug : compile le main avec l'option -g pour permettre l'utilisation et génère le résultat de valgrind sur l'éxecutable test et main dans les fichiers ";
	@echo "- document : génère la documentation sous format html et latex dans le dossier doc";
	@echo "- clean : supprime tous les objets créés, le contenu du dossier bin ainsi que toute la documentation créée"
	@echo "- run : lancer le mode terminal"
	@echo "- rungui : lancer le mode graphique"

#Différent exécutable et objets
$(BINDIR)/exec: $(OBJDIR)/main.o $(OBJDIR)/init.o $(OBJDIR)/list.o $(OBJDIR)/grid.o $(OBJDIR)/pile.o $(OBJDIR)/recouvrement.o $(OBJDIR)/affichage.o $(OBJDIR)/score.o $(OBJDIR)/solver.o
	$(CC) $^ -o $@

$(OBJDIR)/recouvrement.o: $(SRCDIR)/recouvrement.c $(SRCDIR)/init.h $(SRCDIR)/pile.h
	$(CC) -c $< -o $@

$(OBJDIR)/solver.o: $(SRCDIR)/solver.c $(SRCDIR)/score.h
	$(CC) -c $< -o $@

$(OBJDIR)/score.o: $(SRCDIR)/score.c $(SRCDIR)/recouvrement.h
	$(CC) -c $< -o $@

$(OBJDIR)/affichage.o: $(SRCDIR)/affichage.c $(SRCDIR)/init.h
	$(CC) -c $< -o $@

$(OBJDIR)/main.o: $(SRCDIR)/main.c $(SRCDIR)/init.h
	$(CC) -c $< -o $@

$(OBJDIR)/init.o: $(SRCDIR)/init.c $(SRCDIR)/list.h
	$(CC) -c $< -o $@

$(OBJDIR)/list.o: $(SRCDIR)/list.c $(SRCDIR)/grid.h
	$(CC) -c $< -o $@

$(OBJDIR)/grid.o: $(SRCDIR)/grid.c $(SRCDIR)/pile.h
	$(CC) -c $< -o $@

$(OBJDIR)/pile.o: $(SRCDIR)/pile.c
	$(CC) -c $< -o $@

$(OBJDIR)/graphic3.o: $(SRCDIR)/graphic3.c $(SRCDIR)/score.h $(SRCDIR)/init.h $(SRCDIR)/affichage.h $(SRCDIR)/recouvrement.h $(SRCDIR)/solver.h
	$(CC) -c $< -o $@

$(BINDIR)/test: $(TESTDIR)/test.c $(OBJDIR)/init.o $(OBJDIR)/list.o $(OBJDIR)/grid.o $(OBJDIR)/pile.o $(OBJDIR)/affichage.o $(OBJDIR)/recouvrement.o $(OBJDIR)/score.o
	$(CC)  $^ -o $(BINDIR)/test -lcunit

$(BINDIR)/interface: src/graphic3.c src/pile.c src/init.c src/list.c src/grid.c src/score.c src/affichage.c src/recouvrement.c src/solver.c
	$(CCGTK) $^ `pkg-config --cflags --libs gtk+-3.0` -o $(BINDIR)/interface

#Différente commande
#Compiler et créer le main
compile: $(BINDIR)/exec $(BINDIR)/interface
#Nettoyage intégral
clean:
	@rm -r $(OBJDIR)/*.o $(BINDIR)/*.txt $(BINDIR)/test $(BINDIR)/temp.txt $(DOCDIR)/html $(DOCDIR)/latex $(BINDIR)/exec $(TESTDIR)/temp.txt temp.txt $(BINDIR)/debug $(DOCDIR)/Rapport.aux $(DOCDIR)/Rapport.log $(DOCDIR)/Rapport.toc $(FILEDIR)/debugtest.txt $(FILEDIR)/debugmain.txt *.txt > /dev/null 2>&1

#Génération des documents
document:
	@cd doc ; doxygen Doxyfile
#Compilation des tests
test: $(BINDIR)/test

#Compilation pour fuites de mémoire
debug:
	make compile; make test; valgrind  -v --leak-check=full --show-leak-kinds=all --track-origins=yes bin/test > file/debugtest.txt 2>&1; valgrind  -v --leak-check=full --show-leak-kinds=all --track-origins=yes bin/test > file/debugmain.txt 2>&1;

#Lancer le jeu en mode terminal
run:
	./bin/exec

#Lancer le jeu en mode graphique
rungui:
	./bin/interface

